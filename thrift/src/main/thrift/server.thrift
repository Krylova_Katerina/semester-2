namespace java ru.omsu.imit.thrift

exception TodoListException {
  1: string errorString,
}


struct TodoItem{
        1: required i32 id;
	2: required string text;
	3: required string surname;
	4: required string name;
}

struct TodoItemList {
	1: list<TodoItem> items
}

service TodoItemService {
        TodoItem add(1:TodoItem item);
   	TodoItemList getAll(); 
	TodoItem getById(1: i32 id) throws (1: TodoListException ex);
	void deleteById(1: i32 id) throws (1: TodoListException ex);
	TodoItem editById(1: i32 id, 2: string text, 3: string surname, 4: string name) throws (1: TodoListException ex);
    list<TodoItem> getByFullName(1: string surname; 2: string name) throws (1: TodoListException ex);
}

