package ru.omsu.imit.thrift.serviceimpl;

import org.apache.thrift.TException;

import ru.omsu.imit.thrift.TodoItem;
import ru.omsu.imit.thrift.TodoItemList;
import ru.omsu.imit.thrift.TodoItemService;
import ru.omsu.imit.thrift.TodoListException;
import ru.omsu.imit.thrift.TodoItemService.Iface;
import ru.omsu.imit.thrift.dao.TodoDAO;
import ru.omsu.imit.thrift.daoimpl.TodoDAOImpl;

import java.util.List;

public class TodoItemServiceImpl implements TodoItemService.Iface {
	
	private TodoDAO todoDAO = new TodoDAOImpl();
	
	@Override
	public TodoItem add(TodoItem item) throws TException {
		TodoItem addedItem = todoDAO.addItem(item);
		return addedItem;
	}

	@Override
	public TodoItem getById(int id) throws TodoListException, TException {
		return todoDAO.getById(id);
		
	}

	@Override
	public TodoItemList getAll() throws TException {
		return todoDAO.getAll();
	}

	@Override
	public void deleteById(int id) throws TodoListException, TException {
		todoDAO.deleteById(id);
	}

	@Override
	public List<TodoItem> getByFullName(String surname, String name) throws TodoListException, TException {
		return todoDAO.getByFullName(surname, name);
	}

	@Override
	public TodoItem editById(int id, String text, String surname, String name) throws TodoListException, TException {
		return todoDAO.editItem(id, text, surname, name);
	}



	


}