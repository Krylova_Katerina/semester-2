package ru.omsu.imit.thrift.database;

import ru.omsu.imit.thrift.TodoItem;
import ru.omsu.imit.thrift.TodoItemList;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class DataBaseEmulator {
	private static List<TodoItem> list = new ArrayList<>();
	private static AtomicInteger atomicInteger = new AtomicInteger(1);

	public static void addItem(TodoItem item) {
		item.setId(atomicInteger.getAndIncrement());
		synchronized (list) {
			list.add(item);
		}
	}

	public static TodoItem getById(int id) {
		synchronized (list) {
			for (TodoItem item : list) {
				if (item.getId() == id)
					return item;
			}
			return null;
		}
	}

	public static TodoItemList getAll() {
		synchronized (list) {
			List<TodoItem> copyList = new ArrayList<>();
			for (TodoItem item : list) {
				TodoItem copyItem = new TodoItem(item.getId(), item.getText(),item.getSurname(), item.getName());
				copyList.add(copyItem);
			}
			return new TodoItemList(copyList);
		}
	}

	public static boolean deleteById(int id) {
		synchronized (list) {
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i).getId() == id) {
					list.remove(i);
					return true;
				}
			}
			return false;
		}
	}

	public static boolean editItemById(int id, String newText, String newSurname, String newName) {
		synchronized (list) {
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i).getId() == id) {
					list.get(i).setText(newText);
					list.get(i).setSurname(newSurname);
					list.get(i).setName(newName);
					return true;
				}
			}
			return false;
		}
	}

	public static List<TodoItem> getByFullName(String surname, String name) {
		List<TodoItem> items = new ArrayList<>();
		synchronized (list) {
			for (TodoItem item : list) {
				if (item.getSurname().equals(surname) && item.getName().equals(name))
					items.add(item);
			}
			return items;
		}
	}

	public static void clear() {
		synchronized (list) {
			list.clear();
		}

	}

}
