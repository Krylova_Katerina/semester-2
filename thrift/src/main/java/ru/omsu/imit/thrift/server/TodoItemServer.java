package ru.omsu.imit.thrift.server;

import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TSimpleServer;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TServerTransport;

import ru.omsu.imit.thrift.TodoItemService;
import ru.omsu.imit.thrift.TodoItemService.Iface;
import ru.omsu.imit.thrift.TodoItemService.Processor;
import ru.omsu.imit.thrift.serviceimpl.TodoItemServiceImpl;


public class TodoItemServer {

	public static void StartsSimpleServer(TodoItemService.Processor<TodoItemService.Iface> processor) {
		try {

			TServerTransport serverTransport = new TServerSocket(9090);
			TServer server = new TSimpleServer(new TServer.Args(serverTransport).processor(processor));
			
			// Use this for a multithreaded server
			// TServer server = new TThreadPoolServer(new TThreadPoolServer.Args(serverTransport).processor(processor));
			
			System.out.println("Starting TodoItem server...");
			server.serve();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		TodoItemService.Iface userService = new TodoItemServiceImpl();
		TodoItemService.Processor<TodoItemService.Iface> processor = new TodoItemService.Processor<TodoItemService.Iface>(userService);
		StartsSimpleServer(processor);
	}
}