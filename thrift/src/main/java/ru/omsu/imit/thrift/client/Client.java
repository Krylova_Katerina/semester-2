package ru.omsu.imit.thrift.client;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;

import ru.omsu.imit.thrift.TodoItem;
import ru.omsu.imit.thrift.TodoItemList;
import ru.omsu.imit.thrift.TodoItemService;
import ru.omsu.imit.thrift.TodoListException;

public class Client {

	private static TodoItemService.Client client;

	public static void main(String[] args) {
		try (TTransport transport = new TSocket("localhost", 9090)) {
			transport.open();
			TProtocol protocol = new TBinaryProtocol(transport);
			client = new TodoItemService.Client(protocol);

			addItem("матанализ", "Роман", "Елена");
			addItem("программирование","Васильев", "Василий");
			addItem("физика","Пупкин", "Василий");
			addItem("философия","Сидоров","Владислав");

			//System.out.println("======================= getAllItems =======================");
			getAllItems();

			editItem(4, "филология","Сидоров","Владислав");

			try {
				//System.out.println("=======================  not found ======================= ");
				editItem(5, "психология","Сидоров","Владислав");
			} catch (TodoListException ex) {
				System.out.println(ex.getErrorString());
			}

			//System.out.println("======================= getAllItems =======================");
			getAllItems();

			deleteItem(4);

			try {
				//System.out.println("======================= cant'delete ======================= ");
				deleteItem(5);
			} catch (TodoListException ex) {
				System.out.println(ex.getErrorString());
			}

			//System.out.println("======================= getAllItems =======================");
			getAllItems();

		} catch (TTransportException e) {
			e.printStackTrace();
		} catch (TException x) {
			x.printStackTrace();
		}
	}

	private static void addItem(String text, String surname, String name) throws TException {
		TodoItem item = client.add(new TodoItem(0, text, surname, name));
		System.out.println(item.getSurname() + " " + item.getName() + " " + item.getText() + " " + item.getId());
	}

	private static void getAllItems() throws TException {
		TodoItemList list = client.getAll();
		for (TodoItem item : list.getItems()) {
			System.out.println(item.getSurname() + " " + item.getName() + " " + item.getText() + " " + item.getId());
		}
	}

	private static void editItem(int id, String text, String surname, String name) throws TodoListException, TException {
		client.editById(id, text, surname, name);
	}

	private static void deleteItem(int id) throws TodoListException, TException {
		client.deleteById(id);
	}

}