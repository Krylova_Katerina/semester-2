package ru.omsu.imit.workwithsql;


import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static ru.omsu.imit.workwithsql.Jbdc.*;

public class JbdcTest {

    @Before
    public void testInnerBooks(){
        if (!loadDriver()) System.out.println(" ");
        Jbdc jbdc= new Jbdc();
        try (Connection con = DriverManager.getConnection(getURL(), getUSER(), getPASSWORD())) {
            changeBooks(con,"DELETE FROM books" );
            insertBooks(con, "INSERT INTO books VALUES(NULL,'Автостопом по галактике',1990,200,'АБС', 'твердый')");
            insertBooks(con, "INSERT INTO books VALUES(NULL,'Welcome',1800,2400,'ААА', 'мягкий')");
            insertBooks(con, "INSERT INTO books VALUES(NULL,'emocleW',1850,300,'БББ', 'без переплета')");
            insertBooks(con, "INSERT INTO books VALUES(NULL,'Жук в муравейнике',2005,190,'ЕВС', 'твердый')");
            insertBooks(con, "INSERT INTO books VALUES(NULL,'Wood',1745,400,'ЕВС', 'твердый')");
        } catch (SQLException sqlEx) {
            fail("SQLException");
        }
    }


    @Test
    public void testSelectAll() {
        if (!loadDriver())
            return;
        String selectFromBooks = " SELECT * FROM books";
        try (Connection con = DriverManager.getConnection(getURL(), getUSER(), getPASSWORD())) {
            List<Book> myBooks = new ArrayList<>();
            myBooks.add(new Book(1, "Автостопом по галактике",1990,200,"АБС", "твердый"));
            myBooks.add(new Book(2, "Welcome",1800,2400,"ААА", "мягкий"));
            myBooks.add(new Book(3, "emocleW",1850,300,"БББ", "без переплета"));
            myBooks.add(new Book(4, "Жук в муравейнике",2005,190,"ЕВС", "твердый"));
            myBooks.add(new Book(5, "Wood",1745,400,"ЕВС", "твердый"));

            List<Book> result = getBooks(con, selectFromBooks);
            Collections.sort(result, (o1, o2) -> o1.getId() - o2.getId());

            assertEquals(result, myBooks);
        } catch (SQLException sqlEx) {
            fail("SQLException");
        }
    }

    @Test
    public void testSelectID() {
        if (!loadDriver())
            return;
        try (Connection con = DriverManager.getConnection(getURL(), getUSER(), getPASSWORD())) {
            List<Book> myBooks = new ArrayList<>();
            myBooks.add(new Book(1, "Автостопом по галактике",1990,200,"АБС", "твердый"));
            myBooks.add(new Book(2, "Welcome",1800,2400,"ААА", "мягкий"));

            List<Book> result = getBooks(con, " SELECT * FROM books WHERE id<3");;
            Collections.sort(result, (o1, o2) -> o1.getId() - o2.getId());
            assertEquals(result, myBooks);

        } catch (SQLException sqlEx) {
            fail("SQLException");
        }
    }

    @Test
    public void testSelectID2() {
        if (!loadDriver())
            return;
        try (Connection con = DriverManager.getConnection(getURL(), getUSER(), getPASSWORD())) {
            List<Book> myBooks = new ArrayList<>();
            myBooks.add(new Book(3, "emocleW",1850,300,"БББ", "без переплета"));
            List<Book> result = getBooks(con, "  SELECT * FROM books WHERE id=3");
            Collections.sort(result, (o1, o2) -> o1.getId() - o2.getId());
            assertEquals(result, myBooks);

        } catch (SQLException sqlEx) {
            fail("SQLException");
        }
    }

    @Test
    public void testSelectTitle() {
        if (!loadDriver())
            return;
        try (Connection con = DriverManager.getConnection(getURL(), getUSER(), getPASSWORD())) {

            List<Book> myBooks = new ArrayList<>();
            myBooks.add(new Book(2, "Welcome",1800,2400,"ААА", "мягкий"));
            List<Book> result = getBooks(con,"SELECT * FROM books WHERE title LIKE 'Welcome'");
            Collections.sort(result, (o1, o2) -> o1.getId() - o2.getId());

            assertEquals(result, myBooks);
        } catch (SQLException sqlEx) {
            fail("SQLException");
        }
    }

    @Test
    public void testSelectTitleW() {
        if (!loadDriver())
            return;
        try (Connection con = DriverManager.getConnection(getURL(), getUSER(), getPASSWORD())) {
            List<Book> myBooks = new ArrayList<>();
            myBooks.add(new Book(2, "Welcome",1800,2400,"ААА", "мягкий"));
            myBooks.add(new Book(5, "Wood",1745,400,"ЕВС", "твердый"));
            List<Book> result = getBooks(con,"SELECT * FROM books WHERE title LIKE 'W%'");
            Collections.sort(result, (o1, o2) -> o1.getId() - o2.getId());

            assertEquals(result, myBooks);

        } catch (SQLException sqlEx) {
            fail("SQLException");
        }
    }

    @Test
    public void testSelectTitleW2() {
        if (!loadDriver())
            return;
        try (Connection con = DriverManager.getConnection(getURL(), getUSER(), getPASSWORD())) {
            List<Book> myBooks = new ArrayList<>();
            myBooks.add(new Book(3, "emocleW",1850,300,"БББ", "без переплета"));
            List<Book> result = getBooks(con,"SELECT * FROM books WHERE title LIKE '%W'");
            Collections.sort(result, (o1, o2) -> o1.getId() - o2.getId());

            assertEquals(result, myBooks);
        } catch (SQLException sqlEx) {
            fail("SQLException");
        }
    }

    @Test
    public void testSelectYear() {
        if (!loadDriver())
            return;
        try (Connection con = DriverManager.getConnection(getURL(), getUSER(), getPASSWORD())) {
            List<Book> myBooks = new ArrayList<>();
            myBooks.add(new Book(1, "Автостопом по галактике",1990,200,"АБС", "твердый"));
            myBooks.add(new Book(2, "Welcome",1800,2400,"ААА", "мягкий"));
            myBooks.add(new Book(3, "emocleW",1850,300,"БББ", "без переплета"));
            myBooks.add(new Book(5, "Wood",1745,400,"ЕВС", "твердый"));

            List<Book> result = getBooks(con, "SELECT * FROM books WHERE (year_publ>=1700 and year_publ<=1990)");
            Collections.sort(result, (o1, o2) -> o1.getId() - o2.getId());

            assertEquals(result, myBooks);
        } catch (SQLException sqlEx) {
            fail("SQLException");
        }
    }

    @Test
    public void testSelectPages() {
        if (!loadDriver())
            return;
        try (Connection con = DriverManager.getConnection(getURL(), getUSER(), getPASSWORD())) {
            List<Book> myBooks = new ArrayList<>();
            myBooks.add(new Book(1, "Автостопом по галактике",1990,200,"АБС", "твердый"));
            myBooks.add(new Book(3, "emocleW",1850,300,"БББ", "без переплета"));
            myBooks.add(new Book(4, "Жук в муравейнике",2005,190,"ЕВС", "твердый"));
            myBooks.add(new Book(5, "Wood",1745,400,"ЕВС", "твердый"));

            List<Book> result = getBooks(con, "SELECT * FROM books WHERE (pages >=190 and pages<=500)");
            Collections.sort(result, (o1, o2) -> o1.getId() - o2.getId());

            assertEquals(result, myBooks);
        } catch (SQLException sqlEx) {
            fail("SQLException");
        }
    }

    @Test
    public void testSelectPagesAndYear() {
        if (!loadDriver())
            return;
        try (Connection con = DriverManager.getConnection(getURL(), getUSER(), getPASSWORD())) {
            List<Book> myBooks = new ArrayList<>();
            myBooks.add(new Book(1, "Автостопом по галактике",1990,200,"АБС", "твердый"));
            myBooks.add(new Book(3, "emocleW",1850,300,"БББ", "без переплета"));
            myBooks.add(new Book(5, "Wood",1745,400,"ЕВС", "твердый"));

            List<Book> result = getBooks(con, "SELECT * FROM books WHERE (pages >=190 and pages<=500 and year_publ>=1700 and year_publ<=1990)");
            Collections.sort(result, (o1, o2) -> o1.getId() - o2.getId());

            assertEquals(result, myBooks);
        } catch (SQLException sqlEx) {
            fail("SQLException");
        }
    }

    /*=====4=====*/

    @Test
    public void testUpdatePages() {
        if (!loadDriver())
            return;
        try (Connection con = DriverManager.getConnection(getURL(), getUSER(), getPASSWORD())) {
            List<Book> myBooks = new ArrayList<>();
            myBooks.add(new Book(1, "Автостопом по галактике",1990,200,"АБС", "твердый"));
            myBooks.add(new Book(2, "Welcome",1800,200,"ААА", "мягкий"));
            myBooks.add(new Book(3, "emocleW",1850,200,"БББ", "без переплета"));
            myBooks.add(new Book(4, "Жук в муравейнике",2005,200,"ЕВС", "твердый"));
            myBooks.add(new Book(5, "Wood",1745,200,"ЕВС", "твердый"));

            changeBooks(con,"UPDATE books SET pages=200");
            List<Book> result = getBooks(con,"SELECT * FROM books");
            Collections.sort(result, (o1, o2) -> o1.getId() - o2.getId());

            assertEquals(result, myBooks);
        } catch (SQLException sqlEx) {
            fail("SQLException");
        }
    }

    @Test
    public void testUpdatePages2() {
        if (!loadDriver())
            return;
        try (Connection con = DriverManager.getConnection(getURL(), getUSER(), getPASSWORD())) {
            List<Book> myBooks = new ArrayList<>();
            myBooks.add(new Book(1, "Автостопом по галактике",1990,200,"АБС", "твердый"));
            myBooks.add(new Book(2, "Welcome",1800,100,"ААА", "мягкий"));
            myBooks.add(new Book(3, "emocleW",1850,300,"БББ", "без переплета"));
            myBooks.add(new Book(4, "Жук в муравейнике",2005,190,"ЕВС", "твердый"));
            myBooks.add(new Book(5, "Wood",1745,100,"ЕВС", "твердый"));


            changeBooks(con,"UPDATE books SET pages=100 WHERE pages>350");
            List<Book> result = getBooks(con,"SELECT * FROM books");
            Collections.sort(result, (o1, o2) -> o1.getId() - o2.getId());

            assertEquals(result, myBooks);

        } catch (SQLException sqlEx) {
            fail("SQLException");
        }
    }

    @Test
    public void testUpdatePages3() {
        if (!loadDriver())
            return;
        try (Connection con = DriverManager.getConnection(getURL(), getUSER(), getPASSWORD())) {
            List<Book> myBooks = new ArrayList<>();
            myBooks.add(new Book(1, "Автостопом по галактике",1990,200,"АБС", "твердый"));
            myBooks.add(new Book(2, "Welcome",1800,100,"ААА", "мягкий"));
            myBooks.add(new Book(3, "emocleW",1850,190,"БББ", "без переплета"));
            myBooks.add(new Book(4, "Жук в муравейнике",2005,190,"ЕВС", "твердый"));
            myBooks.add(new Book(5, "Wood",1745,100,"ЕВС", "твердый"));

            changeBooks(con,"UPDATE books SET pages=100 WHERE pages>350 AND year_publ<1900");
            List<Book> result = getBooks(con,"SELECT * FROM books");
            Collections.sort(result, (o1, o2) -> o1.getId() - o2.getId());

            assertEquals(result, myBooks);

        } catch (SQLException sqlEx) {
            fail("SQLException");
        }
    }

    @Test
    public void testUpdatePages4() {
        if (!loadDriver())
            return;
        try (Connection con = DriverManager.getConnection(getURL(), getUSER(), getPASSWORD())) {
            List<Book> myBooks = new ArrayList<>();
            myBooks.add(new Book(1, "Автостопом по галактике",1990,200,"АБС", "твердый"));
            myBooks.add(new Book(2, "Welcome",1800,100,"ААА", "мягкий"));
            myBooks.add(new Book(3, "emocleW",1850,300,"БББ", "без переплета"));
            myBooks.add(new Book(4, "Жук в муравейнике",2005,190,"ЕВС", "твердый"));
            myBooks.add(new Book(5, "Wood",1745,100,"ЕВС", "твердый"));

            changeBooks(con,"UPDATE books SET pages=100 WHERE pages>350 AND year_publ<1900 AND title LIKE 'W%'");
            List<Book> result = getBooks(con,"SELECT * FROM books");
            Collections.sort(result, (o1, o2) -> o1.getId() - o2.getId());

            assertEquals(result, myBooks);
        } catch (SQLException sqlEx) {
            fail("SQLException");
        }
    }

    @Test
    public void testDeletePages() {
        if (!loadDriver())
            return;
        try (Connection con = DriverManager.getConnection(getURL(), getUSER(), getPASSWORD())) {
            List<Book> myBooks = new ArrayList<>();
            myBooks.add(new Book(1, "Автостопом по галактике",1990,200,"АБС", "твердый"));
            myBooks.add(new Book(3, "emocleW",1850,300,"БББ", "без переплета"));
            myBooks.add(new Book(4, "Жук в муравейнике",2005,190,"ЕВС", "твердый"));

            changeBooks(con,"DELETE FROM books WHERE pages>350");
            List<Book> result = getBooks(con,"SELECT * FROM books");
            Collections.sort(result, (o1, o2) -> o1.getId() - o2.getId());

            assertEquals(result, myBooks);
            changeBooks(con, "DELETE FROM books WHERE pages>350");
        } catch (SQLException sqlEx) {
            fail("SQLException");
        }
    }

    @Test
    public void testDeleteTitle() {
        if (!loadDriver())
            return;
        try (Connection con = DriverManager.getConnection(getURL(), getUSER(), getPASSWORD())) {
            List<Book> myBooks = new ArrayList<>();
            myBooks.add(new Book(1, "Автостопом по галактике",1990,200,"АБС", "твердый"));
            myBooks.add(new Book(3, "emocleW",1850,300,"БББ", "без переплета"));
            myBooks.add(new Book(4, "Жук в муравейнике",2005,190,"ЕВС", "твердый"));


            changeBooks(con,"DELETE FROM books WHERE title LIKE 'W%'");
            List<Book> result = getBooks(con,"SELECT * FROM books");
            Collections.sort(result, (o1, o2) -> o1.getId() - o2.getId());

            assertEquals(result, myBooks);
        } catch (SQLException sqlEx) {
            fail("SQLException");
        }
    }

    @Test
    public void testDeleteId() {
        if (!loadDriver())
            return;
        try (Connection con = DriverManager.getConnection(getURL(), getUSER(), getPASSWORD())) {
            List<Book> myBooks = new ArrayList<>();
            myBooks.add(new Book(1, "Автостопом по галактике",1990,200,"АБС", "твердый"));
            myBooks.add(new Book(2, "Welcome",1800,2400,"ААА", "мягкий"));


            changeBooks(con,"DELETE FROM books WHERE id>=3 AND id<=5");
            List<Book> result = getBooks(con,"SELECT * FROM books");
            Collections.sort(result, (o1, o2) -> o1.getId() - o2.getId());

            assertEquals(result, myBooks);
        } catch (SQLException sqlEx) {
            fail("SQLException");
        }
    }

    @Test
    public void testDeleteTitle1() {
        if (!loadDriver())
            return;
        try (Connection con = DriverManager.getConnection(getURL(), getUSER(), getPASSWORD())) {
            List<Book> myBooks = new ArrayList<>();
            myBooks.add(new Book(1, "Автостопом по галактике",1990,200,"АБС", "твердый"));
            myBooks.add(new Book(3, "emocleW",1850,300,"БББ", "без переплета"));
            myBooks.add(new Book(4, "Жук в муравейнике",2005,190,"ЕВС", "твердый"));
            myBooks.add(new Book(5, "Wood",1745,400,"ЕВС", "твердый"));

            changeBooks(con,"DELETE FROM books WHERE title LIKE 'Welcome'");
            List<Book> result = getBooks(con,"SELECT * FROM books");
            Collections.sort(result, (o1, o2) -> o1.getId() - o2.getId());

            assertEquals(result, myBooks);
        } catch (SQLException sqlEx) {
            fail("SQLException");
        }
    }

    @Test
    public void testDeleteTitle2() {
        if (!loadDriver())
            return;
        try (Connection con = DriverManager.getConnection(getURL(), getUSER(), getPASSWORD())) {
            List<Book> myBooks = new ArrayList<>();
            myBooks.add(new Book(1, "Автостопом по галактике",1990,200,"АБС", "твердый"));
            myBooks.add(new Book(3, "emocleW",1850,300,"БББ", "без переплета"));
            myBooks.add(new Book(4, "Жук в муравейнике",2005,190,"ЕВС", "твердый"));


            changeBooks(con,"DELETE FROM books WHERE title in ('Welcome', 'Groot', 'Wood')");
            List<Book> result = getBooks(con,"SELECT * FROM books");
            Collections.sort(result, (o1, o2) -> o1.getId() - o2.getId());

            assertEquals(result, myBooks);
        } catch (SQLException sqlEx) {
            fail("SQLException");
        }
    }

    @Test
    public void testDeleteTitle3() {
        if (!loadDriver())
            return;
        try (Connection con = DriverManager.getConnection(getURL(), getUSER(), getPASSWORD())) {
            List<Book> myBooks = new ArrayList<>();

            changeBooks(con,"DELETE FROM books ");
            List<Book> result = getBooks(con,"SELECT * FROM books");
            Collections.sort(result, (o1, o2) -> o1.getId() - o2.getId());
            assertEquals(result, myBooks);
        } catch (SQLException sqlEx) {
            fail("SQLException");
        }
    }
    @Test
    public void testUpdateBinding() {
        if (!loadDriver())
            return;
        try (Connection con = DriverManager.getConnection(getURL(), getUSER(), getPASSWORD())) {
            List<Book> myBooks = new ArrayList<>();
            myBooks.add(new Book(1, "Автостопом по галактике",1990,200,"АБС", "без переплета"));
            myBooks.add(new Book(2, "Welcome",1800,2400,"ААА", "без переплета"));
            myBooks.add(new Book(3, "emocleW",1850,300,"БББ", "без переплета"));
            myBooks.add(new Book(4, "Жук в муравейнике",2005,190,"ЕВС", "без переплета"));
            myBooks.add(new Book(5, "Wood",1745,400,"ЕВС", "без переплета"));

            changeBooks(con,"UPDATE books SET binding= 'без переплета'");
            List<Book> result = getBooks(con,"SELECT * FROM books");
            Collections.sort(result, (o1, o2) -> o1.getId() - o2.getId());

            assertEquals(result, myBooks);;
        } catch (SQLException sqlEx) {
            fail("SQLException");
        }
    }

    @Test
    public void testUpdatepuBlishingHouse() {
        if (!loadDriver())
            return;
        try (Connection con = DriverManager.getConnection(getURL(), getUSER(), getPASSWORD())) {
            List<Book> myBooks = new ArrayList<>();
            myBooks.add(new Book(1, "Автостопом по галактике",1990,200,"АБС", "твердый"));
            myBooks.add(new Book(2, "Welcome",1800,2400,"ААА", "мягкий"));
            myBooks.add(new Book(3, "emocleW",1850,300,"ОГО", "без переплета"));
            myBooks.add(new Book(4, "Жук в муравейнике",2005,190,"ОГО", "твердый"));
            myBooks.add(new Book(5, "Wood",1745,400,"ОГО", "твердый"));

            changeBooks(con,"UPDATE books SET publishing_house='ОГО' WHERE id>=3 AND id<=3");
            List<Book> result = getBooks(con,"SELECT * FROM books");
            Collections.sort(result, (o1, o2) -> o1.getId() - o2.getId());

            assertEquals(result, myBooks);
        } catch (SQLException sqlEx) {
            fail("SQLException");
        }
    }

    @Test
    public void testRenameTable() {
        if (!loadDriver())
            return;
        try (Connection con = DriverManager.getConnection(getURL(), getUSER(), getPASSWORD())) {
            rename(con, "RENAME TABLE 'books' TO 'newNameBook'");
        } catch (SQLException sqlEx) {
            fail("SQLException");
        }
    }

    @Test
    public void testRenameColumn() {
        if (!loadDriver())
            return;
        try (Connection con = DriverManager.getConnection(getURL(), getUSER(), getPASSWORD())) {
            rename(con, "ALTER TABLE books CHANGE title bookname");
        } catch (SQLException sqlEx) {
            fail("SQLException");
        }
    }

    @Test
    public void testDeleteTable() {
        if (!loadDriver())
            return;
        try (Connection con = DriverManager.getConnection(getURL(), getUSER(), getPASSWORD())) {
            rename(con, "DROP TABLE IF EXISTS books");
        } catch (SQLException sqlEx) {
            fail("SQLException");
        }
    }
}

