package ru.omsu.imit.todolist.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.todolist.dao.TodoDAO;
import ru.omsu.imit.todolist.daoimpl.TodoDAOImpl;
import ru.omsu.imit.todolist.exception.TodoListException;
import ru.omsu.imit.todolist.model.TodoItem;
import ru.omsu.imit.todolist.rest.request.TodoItemRequest;
import ru.omsu.imit.todolist.rest.response.EmptySuccessResponse;
import ru.omsu.imit.todolist.rest.response.TodoItemInfoResponse;
import ru.omsu.imit.todolist.utils.TodoLIstUtils;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

public class TodoService {

	private static final Logger LOGGER = LoggerFactory.getLogger(TodoService.class);
	private static final Gson GSON = new GsonBuilder().create();
	private TodoDAO todoDAO = new TodoDAOImpl();

	public Response insertTodoItem(String json) {
		LOGGER.debug("Insert todo item " + json);
		try {
			TodoItemRequest request = TodoLIstUtils.getClassInstanceFromJson(GSON, json, TodoItemRequest.class);
			TodoItem item = new TodoItem(request.getText(),request.getName(), request.getLastName());
			TodoItem addedItem = todoDAO.addItem(item);
			String response = GSON.toJson(new TodoItemInfoResponse(addedItem.getId(), addedItem.getText(), addedItem.getName(), addedItem.getLastName()));
			return Response.ok(response, MediaType.APPLICATION_JSON).build();
		} catch (TodoListException ex) {
			return TodoLIstUtils.failureResponse(ex);
		}

	}

	public Response getById(int id) {
		LOGGER.debug("get By id " + id);
		try {
			TodoItem item = todoDAO.getById(id);
			String response = GSON.toJson(new TodoItemInfoResponse(item.getId(), item.getText(), item.getName(), item.getLastName()));
			return Response.ok(response, MediaType.APPLICATION_JSON).build();
		} catch (TodoListException ex) {
			return TodoLIstUtils.failureResponse(ex);
		}
	}

	public Response getAll() {
		LOGGER.debug("get All");
		List<TodoItem> itemList = todoDAO.getAll();
		List<TodoItemInfoResponse> responseList = new ArrayList<>();
		for (TodoItem item : itemList)
			responseList.add(new TodoItemInfoResponse(item.getId(), item.getText(), item.getName(), item.getLastName()));
		String response = GSON.toJson(responseList);
		return Response.ok(response, MediaType.APPLICATION_JSON).build();
	}

	public Response editById(int id, String json) {
		LOGGER.debug("edit By id " + id);
		try {
			TodoItemRequest request = TodoLIstUtils.getClassInstanceFromJson(GSON, json, TodoItemRequest.class);
			TodoItem editedItem = todoDAO.editItem(id, request.getText(),request.getName(), request.getLastName());
			String response = GSON.toJson(new TodoItemInfoResponse(editedItem.getId(), editedItem.getText(), editedItem.getName(), editedItem.getLastName()));
			return Response.ok(response, MediaType.APPLICATION_JSON).build();
		} catch (TodoListException ex) {
			return TodoLIstUtils.failureResponse(ex);
		}
	}

	public Response deleteById(int id) {
		LOGGER.debug("delete By id " + id);
		try {
			todoDAO.deleteById(id);
			String response = GSON.toJson(new EmptySuccessResponse());
			return Response.ok(response, MediaType.APPLICATION_JSON).build();
		} catch (TodoListException ex) {
			return TodoLIstUtils.failureResponse(ex);
		}
	}

}
