package ru.omsu.imit.todolist.daoimpl;


import ru.omsu.imit.todolist.dao.TodoDAO;
import ru.omsu.imit.todolist.database.DataBaseEmulator;
import ru.omsu.imit.todolist.exception.TodoListException;
import ru.omsu.imit.todolist.model.TodoItem;
import ru.omsu.imit.todolist.utils.ErrorCode;

import java.util.List;

public class TodoDAOImpl implements TodoDAO {

	@Override
	public TodoItem addItem(TodoItem item) {
		DataBaseEmulator.addItem(item);
		return item;
	}
	
	@Override
	public TodoItem getById(int id) throws TodoListException {
		TodoItem item = DataBaseEmulator.getById(id);
		if(item == null)
			throw new TodoListException(ErrorCode.ITEM_NOT_FOUND, Integer.toString(id));
		return item;
	}
	
	@Override
	public List<TodoItem> getAll() {
		return DataBaseEmulator.getAll();
	}
	
	
	@Override
	public TodoItem editItem(int id, String newText, String newName, String newLastNAme) throws TodoListException {
		if(!DataBaseEmulator.editItemById(id, newText))
			throw new TodoListException(ErrorCode.ITEM_NOT_FOUND, Integer.toString(id));
		return new TodoItem(id, newText, newName, newLastNAme);
	}
	
	@Override
	public void deleteById(int id) throws TodoListException {
		if(!DataBaseEmulator.deleteById(id))
			throw new TodoListException(ErrorCode.ITEM_NOT_FOUND, Integer.toString(id));
	}

}
