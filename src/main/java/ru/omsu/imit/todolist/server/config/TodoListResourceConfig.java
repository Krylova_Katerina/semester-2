package ru.omsu.imit.todolist.server.config;

import org.glassfish.jersey.server.ResourceConfig;

public class TodoListResourceConfig extends ResourceConfig {
    public TodoListResourceConfig() {
        packages("ru.omsu.imit.todolist.resources",
                "ru.omsu.imit.todolist.rest.mappers");
    }
}
