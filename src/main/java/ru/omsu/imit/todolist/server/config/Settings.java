package ru.omsu.imit.todolist.server.config;

public class Settings {
	
	private static int restHttpPort = 8888;

	public static int getRestHTTPPort() {
		return restHttpPort;
	}

}
