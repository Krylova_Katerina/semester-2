package ru.omsu.imit.todolist.model;

public class TodoItem {

	private int id;
	private String name;
	private String lastName;
	private String text;

	public TodoItem(int id,String text, String name, String lastName) {
		this.id = id;
		this.name = name;
		this.lastName=lastName;
		this.text=text;
	}

	public TodoItem(String text, String name, String lastName) {
		this(0, text, name, lastName);
	}

	public TodoItem() {
	}
	
	public String getText() {
		return text;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setText(String text) {
		this.text = text;
	}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TodoItem todoItem = (TodoItem) o;

        if (id != todoItem.id) return false;
        if (name != null ? !name.equals(todoItem.name) : todoItem.name != null) return false;
        if (lastName != null ? !lastName.equals(todoItem.lastName) : todoItem.lastName != null) return false;
        return text != null ? text.equals(todoItem.text) : todoItem.text == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (text != null ? text.hashCode() : 0);
        return result;
    }
}
