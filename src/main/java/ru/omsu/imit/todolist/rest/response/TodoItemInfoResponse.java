package ru.omsu.imit.todolist.rest.response;

public class TodoItemInfoResponse extends BaseResponseObject{

    private String name;
    private String lastName;
    private int id;
    private String text;
    
    public TodoItemInfoResponse(int id, String text, String name, String lastName) {
		super();
		this.id = id;
		this.name=name;
		this.lastName=lastName;
		this.text=text;
    }

	protected TodoItemInfoResponse() {
    }

	public String getName() {
		return name;
	}

	public String getLastName() {
		return lastName;
	}

	public String getText() {
		return text;
	}

	public int getId() {
		return id;
	}

}