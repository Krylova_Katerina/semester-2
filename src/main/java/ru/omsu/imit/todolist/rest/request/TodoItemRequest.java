package ru.omsu.imit.todolist.rest.request;

public class TodoItemRequest {

    private String text;
    private String name;
    private String lastName;
    
    public TodoItemRequest(String text, String name, String lastName) {
		super();
		this.text = text;
		this.name=name;
		this.lastName=lastName;
    }

	public String getName() {
		return name;
	}

	public String getLastName() {
		return lastName;
	}

	protected TodoItemRequest() {
    }

	public String getText() {
		return text;
	}


}