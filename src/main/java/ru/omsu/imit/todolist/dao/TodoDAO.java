package ru.omsu.imit.todolist.dao;

import ru.omsu.imit.todolist.exception.TodoListException;
import ru.omsu.imit.todolist.model.TodoItem;

import java.util.List;


public interface TodoDAO {

	public TodoItem addItem(TodoItem item);
	public TodoItem getById(int id) throws TodoListException;
	public List<TodoItem> getAll();
	public TodoItem editItem(int id, String newText, String name, String lastName) throws TodoListException;
	public void deleteById(int id) throws TodoListException;


}