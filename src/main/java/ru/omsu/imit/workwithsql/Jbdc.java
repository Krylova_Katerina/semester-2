package ru.omsu.imit.workwithsql;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class Jbdc {

    private static final String URL = "jdbc:mysql://localhost:3306/bookstore";
    private static final String USER = "test";
    private static final String PASSWORD = "test";

    protected static boolean loadDriver() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            return true;
        } catch (ClassNotFoundException e) {
            System.out.println("Error loading JDBC Driver ");
            return false;
        }
    }


    protected static List<Book> getBooks(Connection con, String query) {
        List<Book> result = new ArrayList<>();
        try (Statement stmt = con.createStatement(); ResultSet rs = stmt.executeQuery(query)) {

            while (rs.next()) {
                Book book = new Book();
                book.setId(rs.getInt("id"));
                book.setTitle(rs.getString("title"));
                book.setYear(rs.getInt("year_publ"));
                book.setPages(rs.getInt("pages"));
                book.setPublishingHouse(rs.getString("publishing_house"));
                book.setBinding(rs.getString("binding"));
                result.add(book);
            }
        } catch (SQLException e) {
        }
        return result;
    }


    public static void insertBooks(Connection con, String query) {
        int result;
        try (Statement stmt = con.createStatement()) {
            result = stmt.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
            if (result > 0) {
                ResultSet rs = stmt.getGeneratedKeys();
                if (rs.next()) {
                    int id = rs.getInt(1);
                    System.out.println("id = " + id);
                    rs.close();
                }
            }
        } catch (SQLException e) {

        }

    }

    protected static void changeBooks(Connection con, String query) {
        int result;
        try (Statement stmt = con.createStatement()) {
            result = stmt.executeUpdate(query);

        } catch (SQLException e) {

        }
    }

    protected static void rename(Connection con, String query) {
        try (Statement stmt = con.createStatement()) {
        } catch (SQLException e) {

        }
    }

    public static String getURL() {
        return URL;
    }

    public static String getUSER() {
        return USER;
    }

    public static String getPASSWORD() {
        return PASSWORD;
    }


}