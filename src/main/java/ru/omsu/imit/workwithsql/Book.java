package ru.omsu.imit.workwithsql;

public class Book{
    private int id;
    private String  title;
    private int year_publ;
    private int pages;
    private String publishing_house;
    private String binding;

    public Book(int id, String title, int year_publ, int pages, String publishing_house, String binding) {
        this.id = id;
        this.title = title;
        this.year_publ = year_publ;
        this.pages = pages;
        this.publishing_house = publishing_house;
        this.binding = binding;
    }

    public Book() {

    }

    public  int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year_publ;
    }

    public void setYear(int year_publ) {
        this.year_publ = year_publ;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public String getPublishingHouse() {
        return publishing_house;
    }

    public void setPublishingHouse(String publishing_house) {
        this.publishing_house = publishing_house;
    }

    public String getBinding() {
        return binding;
    }

    public void setBinding(String binding) {
        this.binding = binding;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Book book = (Book) o;

        if (id != book.id) return false;
        if (year_publ != book.year_publ) return false;
        if (pages != book.pages) return false;
        if (title != null ? !title.equals(book.title) : book.title != null) return false;
        if (publishing_house != null ? !publishing_house.equals(book.publishing_house) : book.publishing_house != null)
            return false;
        return binding != null ? binding.equals(book.binding) : book.binding == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + year_publ;
        result = 31 * result + pages;
        result = 31 * result + (publishing_house != null ? publishing_house.hashCode() : 0);
        result = 31 * result + (binding != null ? binding.hashCode() : 0);
        return result;
    }
}
