DROP DATABASE IF EXISTS bookstore;
CREATE DATABASE bookstore;
USE bookstore;

CREATE TABLE AUTHORS (
  id INT(11) NOT NULL AUTO_INCREMENT,
  firstname VARCHAR(50) NOT NULL,
  lastname VARCHAR(50) NOT NULL,
  patronymic VARCHAR(50) DEFAULT NULL,
  birthdate DATE NOT NULL,
  PRIMARY KEY (id),
  KEY firstname (firstname),
  KEY lastname (lastname),
  KEY patronymic (patronymic),
  KEY birthdate (birthdate)
);


CREATE TABLE author_address (
  id INT(11) NOT NULL AUTO_INCREMENT,
  authorid INT(11) NOT NULL,
  email VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY author_FK (authorid,email),
  CONSTRAINT author_address FOREIGN KEY (`authorid`) REFERENCES AUTHORS (id) ON DELETE CASCADE
) ;


CREATE TABLE books (
  id INT(11) NOT NULL AUTO_INCREMENT,
  title VARCHAR(50) NOT NULL,
  year_publ INT(11) NOT NULL,
  pages INT(11) NOT NULL,
  publishing_house VARCHAR(50) NOT NULL, /*издательство*/
  binding  VARCHAR(50) NOT NULL, /*без переплета мягкий твердый*/
  PRIMARY KEY (id),
  KEY YEAR (YEAR),
  KEY pages (pages)
) ;

INSERT INTO books VALUES(NULL,"Автостопом по галактике",1990,200,"АБС", "твердый");
INSERT INTO books VALUES(NULL,"Welcome",1800,2400,"ААА", "мягкий");
INSERT INTO books VALUES(NULL,"emocleW",1850,300,"БББ", "без переплета");
INSERT INTO books VALUES(NULL,"Жук в муравейнике",2005,190,"ЕВС", "твердый");
/*=====3=====*/
/*
SELECT * FROM books;
SELECT * FROM books WHERE id<N;
SELECT * FROM books WHERE id=ID1;
SELECT * FROM books WHERE title LIKE "Название";
SELECT * FROM books WHERE title LIKE "W%";
SELECT * FROM books WHERE title LIKE "%W";
SELECT * FROM books WHERE (year_publ>=Y1 and year_publ<=Y2);
SELECT * FROM books WHERE (pages >=P1 and pages<=P2);
SELECT * FROM books WHERE (pages >=P1 and pages<=P2 and year_publ>=Y1 and year_publ<=Y2);
*/
/*=====4=====*/
/*
UPDATE books SET pages=P;
UPDATE books SET pages=P WHERE pages>P2;
UPDATE books SET pages=P WHERE pages>P2 AND year_publ<Y;
UPDATE books SET pages=P WHERE pages>P2 AND title LIKE "W%";*/
/*=====5=====*/
/*
DELETE FROM books WHERE pages>p;
DELETE FROM books WHERE title LIKE "W%";
DELETE FROM books WHERE id>=ID1 AND id<=ID2;
DELETE FROM books WHERE title LIKE N;
DELETE FROM books WHERE title in (N1, N2, N3);
DELETE FROM books ;*/

/*=====8=====*/
/*UPDATE books SET binding= "без переплета";*/
/*=====9=====*/
/*UPDATE books SET publishing_house=P WHERE id>=ID1 AND id<=ID2;*/
/*=====10=====*/






CREATE TABLE author_book (
  id INT(11) NOT NULL AUTO_INCREMENT,
  authorid INT(11) NOT NULL,
  bookid INT(11) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY author_book (authorid,bookid),
  KEY bookid (bookid),
  FOREIGN KEY (bookid) REFERENCES books (id) ON DELETE CASCADE,
  FOREIGN KEY (authorid) REFERENCES AUTHORS (id) ON DELETE CASCADE
) ;

