package ru.omsu.imit.clientservice;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadPoolBasedMultiThreadServer {
    static int port = 6666;

    public static void main(String[] args) throws Exception {
        ExecutorService executorService = Executors.newCachedThreadPool();
        System.out.println("Server started and ready to accept client requests");
        System.out.println("Hey, guess the number!");
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            int id = 0;
            while (true) {
                Socket clientSocket = serverSocket.accept();
                executorService.execute(new ClientServiceThread(clientSocket, id++));
            }
        }
    }
}
