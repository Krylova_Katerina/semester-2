package ru.omsu.imit.clientservice;


import org.slf4j.LoggerFactory;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Random;


public class ClientServiceThread extends Thread {
    private Socket clientSocket;
    private int clientID;
    private static final org.slf4j.Logger LOGGER =  LoggerFactory.getLogger(ClientServiceThread.class);

    public ClientServiceThread(Socket socket, int id) {
        clientSocket = socket;
        clientID = id;
    }



    public void run() {
        LOGGER.info("Accepted Client : ID - " + clientID + " : Address - " + clientSocket.getInetAddress().getHostName());
        try (DataInputStream in = new DataInputStream(clientSocket.getInputStream());
             DataOutputStream out = new DataOutputStream(clientSocket.getOutputStream())) {
             Random  random = new Random();
             int number = random.nextInt(1000);
            int count = 0;
            while (true) {
                String clientCommand = in.readUTF();
                LOGGER.info("Client " + clientID + " says :" + clientCommand);
                if (clientCommand.equalsIgnoreCase("quit")) {
                    LOGGER.info("Stopping client thread for client : " + clientID);
                    break;
                } else {
                    if (Integer.parseInt(clientCommand) == number) {
                        out.writeUTF("guessed");
                        out.writeUTF("count = "+count);
                        LOGGER.info("guessed. Count = "+count+" Stopping");
                        break;
                    } else {
                        if (Integer.parseInt(clientCommand) < number) {
                            out.writeUTF("few");
                        } else {
                            if (Integer.parseInt(clientCommand) > number) {
                                out.writeUTF("a lot of");
                            }
                        }
                        out.flush();
                        count++;
                    }
                }
            }
        } catch (IOException x) {

            return;
        } finally {
            LOGGER.info("Closing socket");
            try {
                clientSocket.close();
            } catch (IOException e) {
            }
        }

    }
}