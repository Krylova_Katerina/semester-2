package ru.omsu.imit.clientservice;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {
    public static void main(String[] ar) {
        final int serverPort = 6666;
        final String address = "localhost";
        InetAddress ipAddress;
        try {
            ipAddress = InetAddress.getByName(address);
        } catch (UnknownHostException e) {
            return;
        }
        try (Socket socket = new Socket(ipAddress, serverPort);
             DataInputStream in = new DataInputStream(socket.getInputStream());
             DataOutputStream out = new DataOutputStream(socket.getOutputStream());
             BufferedReader keyboardReader = new BufferedReader(new InputStreamReader(System.in, "CP866"))) {

            System.out.println(socket.getLocalPort());
            String line = null;
            System.out.println("Hey, guess the number!");
            System.out.println("Type in number and press enter");

            while (true) {
                line = keyboardReader.readLine();
                System.out.println("Sending this line to the server..." + line);
                out.writeUTF(line);
                out.flush();
                line = in.readUTF();
                if (line.equalsIgnoreCase("guessed")) {
                    System.out.println(line);
                    line= in.readUTF();
                    System.out.println(line);
                    System.out.println("Client stopped");
                    break;
                }
                System.out.println("Server answer is : " + line);
            }
        } catch (IOException e) {
            return;
        }

    }
}
