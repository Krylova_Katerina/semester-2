package ru.omsu.imit.clientservice;

import java.net.ServerSocket;
import java.net.Socket;

public class MultiThreadServer {
    static int port = 6666;

    public static void main(String[] args) throws Exception {
        System.out.println("Server started and ready to accept clients requests");
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            int id = 0;
            while (true) {
                Socket clientSocket = serverSocket.accept();
                ClientServiceThread clientThread = new ClientServiceThread(clientSocket, id++);
                clientThread.start();
            }
        }
    }
}

