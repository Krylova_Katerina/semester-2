-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema store
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `store` ;

-- -----------------------------------------------------
-- Schema store
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `store` DEFAULT CHARACTER SET utf8 ;
USE `store` ;

-- -----------------------------------------------------
-- Table `store`.`idBuyer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `store`.`buyer` ;

CREATE TABLE IF NOT EXISTS `store`.`buyer` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `buyerName` VARCHAR(11) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `store`.`department`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `store`.`department` ;

CREATE TABLE IF NOT EXISTS `store`.`department` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `departmentName` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `store`.`product`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `store`.`product` ;

CREATE TABLE IF NOT EXISTS `store`.`product` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `idDepartment` INT(11) NOT NULL,
  `productName` VARCHAR(50) NOT NULL,
  `productDescription` VARCHAR(50) NULL DEFAULT NULL,
  `quantityInStock` INT(11) NOT NULL,
  `priceForOne` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `idDepartment` (`idDepartment` ASC),
  CONSTRAINT `product_ibfk_1`
    FOREIGN KEY (`idDepartment`)
    REFERENCES `store`.`department` (`id`)
    ON DELETE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `store`.`shoppingcart`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `store`.`shoppingcart` ;

CREATE TABLE IF NOT EXISTS `store`.`shoppingcart` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `idProduct` INT(11) NOT NULL,
  `idBuyer` INT(11) NOT NULL,
  `quantity` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `idProduct` (`idProduct` ASC),
  INDEX `idBuyer` (`idBuyer` ASC),
  CONSTRAINT `shoppingcart_ibfk_1`
    FOREIGN KEY (`idProduct`)
    REFERENCES `store`.`product` (`id`)
    ON DELETE CASCADE,
  CONSTRAINT `shoppingcart_ibfk_2`
    FOREIGN KEY (`idBuyer`)
    REFERENCES `store`.`idBuyer` (`id`)
    ON DELETE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
