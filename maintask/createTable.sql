DROP DATABASE IF EXISTS store;
CREATE DATABASE store;
USE store;

CREATE TABLE departament (
  id INT(11) NOT NULL AUTO_INCREMENT,
  departament_name VARCHAR(50) NOT NULL,
  PRIMARY KEY (id)
) ;


CREATE TABLE product (
  id INT(11) NOT NULL AUTO_INCREMENT,
  id_departament INT(11) NOT NULL,
  product_name VARCHAR(50) NOT NULL,
  product_description VARCHAR(50),
  quantity_in_stock INT(11) NOT NULL,
  price_for_one INT(11) NOT NULL,
  PRIMARY KEY (id_products),
  FOREIGN KEY (id_departament) REFERENCES departament(id) ON DELETE CASCADE /*?*/
) ;

CREATE TABLE shopping_cart (
  id INT(11) NOT NULL AUTO_INCREMENT,
  id_product INT(11) NOT NULL,
  id_buyer INT(11) NOT NULL,
  quantity INT(11) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (id_product) REFERENCES product(id) ON DELETE CASCADE,
  FOREIGN KEY (id_buyer) REFERENCES buyer(id) ON DELETE CASCADE;
  ) ;

CREATE TABLE buyer (
  id INT(11) NOT NULL AUTO_INCREMENT,
  buyer_name VARCHAR(11) NOT NULL,
  PRIMARY KEY (id)
) ;
