package ru.omsu.imit.mybatis;

import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.omsu.imit.mybatis.dao.CommonDAO;
import ru.omsu.imit.mybatis.dao.DepartmentDAO;
import ru.omsu.imit.mybatis.daoimpl.CommonDAOImpl;
import ru.omsu.imit.mybatis.daoimpl.DepartmentDAOImpl;
import ru.omsu.imit.mybatis.model.Department;
import ru.omsu.imit.mybatis.utils.MyBatisUtils;

import static org.junit.Assert.assertEquals;

public class DepartmentDAOTest {
    protected CommonDAO commonDao = new CommonDAOImpl();
    protected DepartmentDAO departmentDAO = new DepartmentDAOImpl();

    @BeforeClass()
    public static void init() {
        Assume.assumeTrue(MyBatisUtils.initSqlSessionFactory());
    }

    @Before()
    public void clearDatabase() {
        commonDao.clear();
    }

    protected void checkFields(Department department, Department department2) {
        assertEquals(department.getDepartmentName(), department2.getDepartmentName());
    }


    @Test
    public void testInsert() {
        Department department = new Department("products");
        departmentDAO.insert(department);
        Department departmentFromDB = departmentDAO.getById(department.getId());
        checkFields(department, departmentFromDB);
    }


}
