package ru.omsu.imit.mybatis;

import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.omsu.imit.mybatis.dao.BuyerDAO;
import ru.omsu.imit.mybatis.dao.CommonDAO;
import ru.omsu.imit.mybatis.daoimpl.BuyerDAOImpl;
import ru.omsu.imit.mybatis.daoimpl.CommonDAOImpl;
import ru.omsu.imit.mybatis.model.Buyer;
import ru.omsu.imit.mybatis.utils.MyBatisUtils;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class BuyerDAOTest {

    protected CommonDAO commonDao = new CommonDAOImpl();
    protected BuyerDAO buyerDAO = new BuyerDAOImpl();

    @BeforeClass()
    public static void init() {
        Assume.assumeTrue(MyBatisUtils.initSqlSessionFactory());
    }

    @Before()
    public void clearDatabase() {
        commonDao.clear();
    }

    protected void checkFields(Buyer buyer, Buyer buyer2) {
        assertEquals(buyer.getBuyerName(), buyer2.getBuyerName());
    }


    @Test
    public void testInsert() {
        Buyer buyer = new Buyer("Marcus");
        buyerDAO.insert(buyer);
        Buyer buyerFromDB = buyerDAO.getById(buyer.getId());
        checkFields(buyer, buyerFromDB);
        }


    @Test
    public void testGetAll() {
        Buyer buyer = new Buyer("Marcus");
        buyerDAO.insert(buyer);

        Buyer buyer2 = new Buyer("Virus");
        buyerDAO.insert(buyer2);

        List<Buyer> buyerFromDB = buyerDAO.getAll();
        assertEquals(2, buyerFromDB.size());

        checkFields(buyer, buyerFromDB.get(0));
        checkFields(buyer2,buyerFromDB.get(1));
    }
}
