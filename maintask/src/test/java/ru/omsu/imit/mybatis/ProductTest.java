package ru.omsu.imit.mybatis;

import org.junit.Test;
import ru.omsu.imit.mybatis.rest.request.DepartmentRequest;
import ru.omsu.imit.mybatis.rest.request.ProductRequest;
import ru.omsu.imit.mybatis.rest.response.DepartmentResponse;
import ru.omsu.imit.mybatis.rest.response.ProductResponse;
import ru.omsu.imit.mybatis.utils.ErrorCode;

public class ProductTest extends MyClientTest {
    @Test
    public void testInsertProduct() {
        DepartmentRequest request = new DepartmentRequest("otdel1");
        DepartmentResponse departmentResponse = addDepartment(request, ErrorCode.SUCCESS);

        ProductRequest requestProduct = new ProductRequest(departmentResponse.getId(), "ball",
                "blue",1234,324);

        addProduct(requestProduct, ErrorCode.SUCCESS);

    }


    @Test
    public void testGetProduct() {
        DepartmentRequest request = new DepartmentRequest("otdel1");
        DepartmentResponse departmentResponse = addDepartment(request, ErrorCode.SUCCESS);

        ProductRequest requestProduct = new ProductRequest(departmentResponse.getId(), "ball",
                "blue",1234,324);

        ProductResponse productResponse = addProduct(requestProduct, ErrorCode.SUCCESS);

        getProductById(productResponse.getProductId(), requestProduct.getDepartmentId(), requestProduct.getProductName(),
                requestProduct.getProductDescription(), requestProduct.getQuantityInStock(), requestProduct.getPriceForOne(),
                ErrorCode.SUCCESS);

    }

    @Test
    public void deleteByIdTest() {
        DepartmentRequest request = new DepartmentRequest("otdel1");
        DepartmentResponse departmentResponse = addDepartment(request, ErrorCode.SUCCESS);

        ProductRequest requestProduct = new ProductRequest(departmentResponse.getId(), "ball",
                "blue",1234,324);

        ProductResponse productResponse = addProduct(requestProduct, ErrorCode.SUCCESS);

        deleteProductById(productResponse.getProductId(), ErrorCode.SUCCESS);
    }

    @Test
    public void getAllTest() {
        DepartmentRequest request = new DepartmentRequest("otdel1");
        DepartmentResponse departmentResponse = addDepartment(request, ErrorCode.SUCCESS);

        ProductRequest requestProduct = new ProductRequest(departmentResponse.getId(), "ball",
                "blue",1234,324);


        addProduct(requestProduct, ErrorCode.SUCCESS);


        ProductRequest requestProduct2 = new ProductRequest(departmentResponse.getId(), "ball",
                "red",123,344);
         addProduct(requestProduct2, ErrorCode.SUCCESS);

        getAllProducts(2, ErrorCode.SUCCESS);
    }



}
