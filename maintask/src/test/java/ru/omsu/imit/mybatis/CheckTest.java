package ru.omsu.imit.mybatis;

import org.junit.Assume;
import org.junit.BeforeClass;
import ru.omsu.imit.mybatis.rest.request.BuyerRequest;
import ru.omsu.imit.mybatis.rest.request.DepartmentRequest;
import ru.omsu.imit.mybatis.rest.request.ProductRequest;
import ru.omsu.imit.mybatis.rest.response.BuyerResponse;
import ru.omsu.imit.mybatis.rest.response.DepartmentResponse;
import ru.omsu.imit.mybatis.rest.response.ProductResponse;
import ru.omsu.imit.mybatis.utils.MyBatisUtils;

import static org.junit.Assert.assertEquals;

public class CheckTest {

    @BeforeClass()
    public static void init() {
        Assume.assumeTrue(MyBatisUtils.initSqlSessionFactory());
    }


    protected void checkProductFields(ProductRequest product, ProductResponse product1) {
        assertEquals(product.getProductName(), product1.getProductName());
        assertEquals(product.getProductDescription(), product1.getProductDescription());
        assertEquals(product.getQuantityInStock(), product1.getQuantityInStock());
        assertEquals(product.getPriceForOne(), product1.getPriceForOne());
    }

    protected void checkDepartmentFields(DepartmentRequest department, DepartmentResponse department1) {
        assertEquals(department.getDepartmentName(), department1.getDepartmentName());
        assertEquals(department.getProductList(), department1.getProductList());
    }

    protected void checkBuyerFields(BuyerRequest buyer, BuyerResponse buyer2) {
        assertEquals(buyer.getBuyerName(), buyer2.getBuyerName());

}


}

