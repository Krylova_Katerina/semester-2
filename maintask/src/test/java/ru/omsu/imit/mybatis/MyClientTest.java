package ru.omsu.imit.mybatis;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.mybatis.client.MyClient;
import ru.omsu.imit.mybatis.rest.request.BuyerRequest;
import ru.omsu.imit.mybatis.rest.request.DepartmentRequest;
import ru.omsu.imit.mybatis.rest.request.ProductRequest;
import ru.omsu.imit.mybatis.rest.response.*;
import ru.omsu.imit.mybatis.server.MyServer;
import ru.omsu.imit.mybatis.server.config.Settings;
import ru.omsu.imit.mybatis.utils.ErrorCode;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MyClientTest extends CheckTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(MyClientTest.class);

    protected static MyClient myClient = new MyClient();
    private static String baseURL;

    private static void initialize() {
        String hostName = null;
        try {
            hostName = InetAddress.getLocalHost().getCanonicalHostName();
        } catch (UnknownHostException e) {
            LOGGER.debug("Can't determine my own host name", e);
        }
        baseURL = "http://localhost:" + Settings.getRestHTTPPort() + "/api";
    }

    @BeforeClass
    public static void startServer() {
        initialize();
        MyServer.createServer();
    }


    @AfterClass
    public static void stopServer() {
        MyServer.stopServer();
    }

   @Before
    public void clearTable() {
       deleteAll(ErrorCode.SUCCESS);
   }


    protected EmptySuccessResponse deleteAll(ErrorCode expectedStatus) {
        Object response = myClient.delete(baseURL + "/clearDB", EmptySuccessResponse.class);
        if (response instanceof EmptySuccessResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            return (EmptySuccessResponse) response;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    public static String getBaseURL() {
        return baseURL;
    }

    protected void checkFailureResponse(Object response, ErrorCode expectedStatus) {
        assertTrue(response instanceof FailureResponse);
        FailureResponse failureResponseObject = (FailureResponse) response;
        assertEquals(expectedStatus, failureResponseObject.getErrorCode());
    }

    /*====================================================================================================================*/

    protected BuyerResponse addBuyer(BuyerRequest request, ErrorCode expectedStatus) {

        Object response = myClient.post(baseURL + "/shop/buyer", request, BuyerResponse.class);
        if (response instanceof BuyerResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            BuyerResponse addBuyerResponse = (BuyerResponse) response;
            checkBuyerFields(request, addBuyerResponse);
            return addBuyerResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected EmptySuccessResponse deleteBuyerById(int id, ErrorCode expectedStatus) {
        Object response = myClient.delete(baseURL + "/shop/buyer/" + id, EmptySuccessResponse.class);
        if (response instanceof EmptySuccessResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            return (EmptySuccessResponse) response;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }


    protected BuyerResponse getBuyerById(int id, String expectedBuyerName, ErrorCode expectedStatus) {
        Object response = myClient.get(baseURL + "/shop/buyer/" + id, BuyerResponse.class);
        if (response instanceof BuyerResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            BuyerResponse getBuyerResponse = (BuyerResponse) response;
           checkBuyerFields(new BuyerRequest(expectedBuyerName), getBuyerResponse);

            assertEquals(expectedBuyerName, getBuyerResponse.getBuyerName());
            return getBuyerResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected List<BuyerResponse> getAllBuyers(int expeectedCount, ErrorCode expectedStatus) {
        Object response = myClient.get(baseURL + "/shop/buyer", List.class);
        if (response instanceof List<?>) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            @SuppressWarnings("unchecked")
            List<BuyerResponse> responseList = (List<BuyerResponse>) response;
            assertEquals(expeectedCount, responseList.size());
            return responseList;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    /*====================================================================================================================*/

    protected DepartmentResponse addDepartment(DepartmentRequest request, ErrorCode expectedStatus) {
         Object response = myClient.post(baseURL + "/shop/department", request, DepartmentResponse.class);
        if (response instanceof DepartmentResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            DepartmentResponse addDepartmentResponse = (DepartmentResponse) response;
            checkDepartmentFields(request, addDepartmentResponse);
            return addDepartmentResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected EmptySuccessResponse deleteDepartmentById(int id, ErrorCode expectedStatus) {
        Object response = myClient.delete(baseURL + "/shop/department/" + id, EmptySuccessResponse.class);
        if (response instanceof EmptySuccessResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            return (EmptySuccessResponse) response;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected DepartmentResponse getDepartmentById(int id, String expectedDepartmentName, ErrorCode expectedStatus) {
        Object response = myClient.get(baseURL + "/shop/department/" + id, DepartmentResponse.class);
        if (response instanceof DepartmentResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            DepartmentResponse getDepartmentResponse = (DepartmentResponse) response;
            checkDepartmentFields(new DepartmentRequest(expectedDepartmentName), getDepartmentResponse);
            return getDepartmentResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected List<DepartmentResponse> getAllDepartments(int expeectedCount, ErrorCode expectedStatus) {
        Object response = myClient.get(baseURL + "/shop/department", List.class);
        if (response instanceof List<?>) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            @SuppressWarnings("unchecked")
            List<DepartmentResponse> responseList = (List<DepartmentResponse>) response;
            assertEquals(expeectedCount, responseList.size());
            return responseList;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    /*======================================================================================================================*/

    protected ProductResponse addProduct(ProductRequest request, ErrorCode expectedStatus) {
         Object response = myClient.post(baseURL + "/shop/product", request, ProductResponse.class);
        if (response instanceof ProductResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            ProductResponse addProductResponse = (ProductResponse) response;
            checkProductFields(request, addProductResponse);
            return addProductResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected EmptySuccessResponse deleteProductById(int id, ErrorCode expectedStatus) {
        Object response = myClient.delete(baseURL + "/shop/product/" + id, EmptySuccessResponse.class);
        if (response instanceof EmptySuccessResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            return (EmptySuccessResponse) response;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected ProductResponse getProductById(int id, int expectedIdDepartment, String expectedProductName,
                                             String expectedProductDescription, int expectedQuantityInStock,
                                             int expectedPriceForOne, ErrorCode expectedStatus) {
        Object response = myClient.get(baseURL + "/shop/product/" + id, ProductResponse.class);
        if (response instanceof ProductResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            ProductResponse getProductResponse = (ProductResponse) response;
            checkProductFields(new ProductRequest(expectedIdDepartment, expectedProductName, expectedProductDescription,
                    expectedQuantityInStock, expectedPriceForOne), getProductResponse);
            return getProductResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected List<ProductResponse> getAllProducts(int expeectedCount, ErrorCode expectedStatus) {
        Object response = myClient.get(baseURL + "/shop/product", List.class);
        if (response instanceof List<?>) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            @SuppressWarnings("unchecked")
            List<ProductResponse> responseList = (List<ProductResponse>) response;
            assertEquals(expeectedCount, responseList.size());
            return responseList;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    /*======================================================================================================================*/


}
