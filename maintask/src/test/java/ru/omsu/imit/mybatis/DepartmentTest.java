package ru.omsu.imit.mybatis;

import org.junit.Test;
import ru.omsu.imit.mybatis.rest.request.DepartmentRequest;
import ru.omsu.imit.mybatis.rest.response.DepartmentResponse;
import ru.omsu.imit.mybatis.utils.ErrorCode;

public class DepartmentTest extends MyClientTest {

    @Test
    public void testInsertDepartment() {
        DepartmentRequest request = new DepartmentRequest("otdel1");

        addDepartment(request, ErrorCode.SUCCESS);
    }


    @Test
    public void testGetDepartment() {
        DepartmentRequest request = new DepartmentRequest("otdel1");
        DepartmentResponse departmentResponse = addDepartment(request, ErrorCode.SUCCESS);

        getDepartmentById(departmentResponse.getId(), request.getDepartmentName(), ErrorCode.SUCCESS);

    }

    @Test
    public void deleteByIdTest() {
        DepartmentRequest request = new DepartmentRequest("otdel1");
        DepartmentResponse departmentResponse = addDepartment(request, ErrorCode.SUCCESS);

        deleteDepartmentById(departmentResponse.getId(), ErrorCode.SUCCESS);
    }




}
