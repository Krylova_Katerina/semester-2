package ru.omsu.imit.mybatis;

import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.omsu.imit.mybatis.dao.CommonDAO;
import ru.omsu.imit.mybatis.dao.DepartmentDAO;
import ru.omsu.imit.mybatis.dao.ProductDAO;
import ru.omsu.imit.mybatis.daoimpl.CommonDAOImpl;
import ru.omsu.imit.mybatis.daoimpl.DepartmentDAOImpl;
import ru.omsu.imit.mybatis.daoimpl.ProductDAOImpl;
import ru.omsu.imit.mybatis.model.Department;
import ru.omsu.imit.mybatis.model.Product;
import ru.omsu.imit.mybatis.utils.MyBatisUtils;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class ProductDAOTest {
    protected CommonDAO commonDao = new CommonDAOImpl();
    protected DepartmentDAO departmentDAO = new DepartmentDAOImpl();
    protected ProductDAO productDAO = new ProductDAOImpl();

    @BeforeClass()
    public static void init() {
        Assume.assumeTrue(MyBatisUtils.initSqlSessionFactory());
    }

    @Before()
    public void clearDatabase() {
        commonDao.clear();
    }

    protected void checkFields(Product product, Product product1) {
        assertEquals(product.getProductName(), product1.getProductName());
        assertEquals(product.getProductDescription(), product1.getProductDescription());
        assertEquals(product.getPriceForOne(), product1.getPriceForOne());
        assertEquals(product.getQuantityInStock(), product1.getQuantityInStock());
    }

    @Test
    public void testInsert() {
        Department department = new Department("products");
        departmentDAO.insert(department);
        Department departmentFromDB = departmentDAO.getById(department.getId());

        Product product = new Product(departmentFromDB.getId(),"ball", "blue", 3423,234);
        productDAO.insert(product);
        Product productFromDB = productDAO.getById(product.getId());
        checkFields(product,productFromDB);

    }

    @Test
    public void testGetAll() {
        departmentDAO.deleteAll();

        Department department = new Department("products");
        departmentDAO.insert(department);
        Department departmentFromDB = departmentDAO.getById(department.getId());

        Product product = new Product(departmentFromDB.getId(),"ball", "blue", 3423,234);
        productDAO.insert(product);

        Product product2 = new Product(departmentFromDB.getId(),"ball", "red", 342,214);
        productDAO.insert(product2);

        List<Product> productsFromDB = productDAO.getAll();
        assertEquals(2, productsFromDB.size());

        checkFields(product, productsFromDB.get(0));
        checkFields(product2,productsFromDB.get(1));
    }



}
