package ru.omsu.imit.mybatis;

import org.junit.Test;
import ru.omsu.imit.mybatis.rest.request.BuyerRequest;
import ru.omsu.imit.mybatis.rest.response.BuyerResponse;
import ru.omsu.imit.mybatis.utils.ErrorCode;

public class BuyerTest extends MyClientTest {

    @Test
    public void testInsertBuyer() {
        BuyerRequest request = new BuyerRequest("loki");
        addBuyer(request, ErrorCode.SUCCESS);
    }

    @Test
    public void testGetBuyer() {
        BuyerRequest request = new BuyerRequest("loki");
        BuyerResponse buyerResponse = addBuyer(request, ErrorCode.SUCCESS);

        getBuyerById(buyerResponse.getId(), request.getBuyerName(), ErrorCode.SUCCESS);

    }

    @Test
    public void deleteByIdTest() {
        BuyerRequest request = new BuyerRequest("loki");
        BuyerResponse buyerResponse = addBuyer(request, ErrorCode.SUCCESS);
        deleteBuyerById(buyerResponse.getId(), ErrorCode.SUCCESS);
    }

    @Test
    public void deleteAll() {
         deleteAll(ErrorCode.SUCCESS);
    }

    @Test
    public void getAllTest() {
        BuyerRequest request = new BuyerRequest("loki");
        addBuyer(request, ErrorCode.SUCCESS);

        BuyerRequest request2 = new BuyerRequest("thor");
        addBuyer(request2, ErrorCode.SUCCESS);

        getAllBuyers(2, ErrorCode.SUCCESS);
    }



}
