package ru.omsu.imit.mybatis.daoimpl;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.mybatis.dao.ProductDAO;
import ru.omsu.imit.mybatis.exception.MyException;
import ru.omsu.imit.mybatis.model.Product;

import java.util.List;

public class ProductDAOImpl extends BaseDAOImpl implements ProductDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(BaseDAOImpl.class);

    @Override
    public Product insert(Product product) {
        LOGGER.debug("DAO Insert product {}", product);
        try (SqlSession sqlSession = getSession()) {
            try {
                getProductMapper(sqlSession).insert(product);
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't insert product {} {}", product, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
        return product;
    }

    @Override
    public void deleteAll() {
        LOGGER.debug("DAO Delete All Product {}");
        try (SqlSession sqlSession = getSession()) {
            try {
                getProductMapper(sqlSession).deleteAll();
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't clear all Product {}", ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public Product getById(int id) {
        try (SqlSession sqlSession = getSession()) {
            return getProductMapper(sqlSession).getById(id);
        } catch (RuntimeException ex) {
            LOGGER.debug("Can't get product by Id {} {}", id, ex);
            throw ex;
        }
    }


    @Override
    public List<Product> getByDepartmentId(int departmentId) {
        try (SqlSession sqlSession = getSession()) {
            return getProductMapper(sqlSession).getByDepartmentId(departmentId);
        } catch (RuntimeException ex) {
            LOGGER.debug("Can't get product {}", ex);
            throw ex;
        }
    }


    public List<Product> getByBuyerId(int buyerId) throws MyException {
        try (SqlSession sqlSession = getSession()) {
            return getProductMapper(sqlSession).getByBuyerId(buyerId);
        } catch (RuntimeException ex) {
            LOGGER.debug("Can't get product {}", ex);
            throw ex;
        }

    }


    @Override
    public List<Product> getAll() {
        try (SqlSession sqlSession = getSession()) {
            return getProductMapper(sqlSession).getAll();
        } catch (RuntimeException ex) {
            LOGGER.debug("can't get all products {}", ex);
            throw ex;
        }
    }


    @Override
    public void deleteById(int id) {

        try (SqlSession sqlSession = getSession()) {
            try {
                getProductMapper(sqlSession).deleteById(id);
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't clear Product witch id {} {}", id, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

}
