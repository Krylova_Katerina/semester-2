package ru.omsu.imit.mybatis.rest.request;

import ru.omsu.imit.mybatis.model.Product;

import java.util.ArrayList;
import java.util.List;

public class DepartmentRequest {
    private String departmentName;
    private List<Product> productList;

    public DepartmentRequest(String departmentName, List<Product> productList) {
        super();
        this.departmentName = departmentName;
        this.productList = productList;
    }

    public DepartmentRequest(String departmentName) {
        this(departmentName, new ArrayList<>());
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public List<Product> getProductList() {
        return productList;
    }
}
