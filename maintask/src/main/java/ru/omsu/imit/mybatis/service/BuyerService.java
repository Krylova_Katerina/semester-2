package ru.omsu.imit.mybatis.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.mybatis.dao.BuyerDAO;
import ru.omsu.imit.mybatis.daoimpl.BuyerDAOImpl;
import ru.omsu.imit.mybatis.exception.MyException;
import ru.omsu.imit.mybatis.model.Buyer;
import ru.omsu.imit.mybatis.rest.request.BuyerRequest;
import ru.omsu.imit.mybatis.rest.response.BuyerResponse;
import ru.omsu.imit.mybatis.rest.response.EmptySuccessResponse;
import ru.omsu.imit.mybatis.utils.JsonUtils;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

public class BuyerService {
    private static final Logger LOGGER = LoggerFactory.getLogger(BuyerService.class);
    private static final Gson GSON = new GsonBuilder().create();
    private BuyerDAO buyerDAO = new BuyerDAOImpl();


    public Response insertBuyer(String json) {
        LOGGER.debug(" Service Insert buyer " + json);
        try {
            BuyerRequest request = JsonUtils.getClassInstanceFromJson(GSON, json, BuyerRequest.class);
            Buyer buyer = buyerDAO.insert(new Buyer(request.getBuyerName(), request.getShoppingCartList()));
            String response = GSON.toJson(new BuyerResponse(buyer.getId(), buyer.getBuyerName(), buyer.getProductList()));
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (MyException ex) {
            return JsonUtils.failureResponse(ex);
        }
    }

    public Response getById(int id) {
        LOGGER.debug("Get buyer by ID " + id);
        Buyer section = buyerDAO.getById(id);
        String response = GSON.toJson(new BuyerResponse(section.getId(), section.getBuyerName(), section.getProductList()));
        return Response.ok(response, MediaType.APPLICATION_JSON).build();

    }

    public Response getAll() {
        LOGGER.debug("get All buyers");
        List<Buyer> buyers = buyerDAO.getAll();
        List<BuyerResponse> responseList = new ArrayList<>();
        for (Buyer buyer : buyers)
            responseList.add(new BuyerResponse(buyer.getId(), buyer.getBuyerName(), buyer.getProductList()));
        String respose = GSON.toJson(responseList);
        return Response.ok(respose, MediaType.APPLICATION_JSON).build();

    }

    public Response deleteById(int id, String json) {
        LOGGER.debug("Delete section by ID");
        buyerDAO.deleteById(id);
        String response = GSON.toJson(new EmptySuccessResponse());
        return Response.ok(response, MediaType.APPLICATION_JSON).build();
    }
}

