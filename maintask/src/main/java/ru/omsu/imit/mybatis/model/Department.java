package ru.omsu.imit.mybatis.model;

import java.util.ArrayList;
import java.util.List;

public class Department {
    private int id;
    private String departmentName;
    private List<Product> productList;

    public Department(int id, String departmentName, List<Product> productList) {
        super();
        this.id = id;
        this.departmentName = departmentName;
        this.productList = productList;
    }

    public Department(String departmentName, List<Product> productList) {
        this(0, departmentName, productList);
    }

    public Department(int id, String departmentName) {
        this(id, departmentName, new ArrayList<>());
    }

    public Department(String departmentName) {
        this(0, departmentName, new ArrayList<>());
    }


    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Department that = (Department) o;

        if (id != that.id) return false;
        if (departmentName != null ? !departmentName.equals(that.departmentName) : that.departmentName != null)
            return false;
        return productList != null ? productList.equals(that.productList) : that.productList == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (departmentName != null ? departmentName.hashCode() : 0);
        result = 31 * result + (productList != null ? productList.hashCode() : 0);
        return result;
    }
}
