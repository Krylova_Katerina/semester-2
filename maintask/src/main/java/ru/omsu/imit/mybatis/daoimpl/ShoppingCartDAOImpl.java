package ru.omsu.imit.mybatis.daoimpl;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.mybatis.dao.ShoppingCartDAO;
import ru.omsu.imit.mybatis.model.Buyer;
import ru.omsu.imit.mybatis.model.Product;

import java.util.List;

public class ShoppingCartDAOImpl extends BaseDAOImpl implements ShoppingCartDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(BaseDAOImpl.class);

    @Override
    public void insert(Product product, Buyer buyer) {
        LOGGER.debug("DAO Insert shopping cart {}", product, buyer);
        try (SqlSession sqlSession = getSession()) {
            try {
                getShoppingCartMapper(sqlSession).insert(product, buyer);
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't insert shopping cart {} {}", product, buyer, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void deleteAll() {
        LOGGER.debug("DAO Delete All Shopping Cart {}");
        try (SqlSession sqlSession = getSession()) {
            try {
                getShoppingCartMapper(sqlSession).deleteAll();
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't clear all Shopping Cart {}", ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }


    @Override
    public List<Product> getByBuyerId(int buyerId) {
        try (SqlSession sqlSession = getSession()) {
            return getShoppingCartMapper(sqlSession).getByBuyerId(buyerId);
        } catch (RuntimeException ex) {
            LOGGER.debug("Can't get shopping cart {}", ex);
            throw ex;
        }
    }

    @Override
    public List<Product> getByProductId(int productId) {
        try (SqlSession sqlSession = getSession()) {
            return getShoppingCartMapper(sqlSession).getByProductId(productId);
        } catch (RuntimeException ex) {
            LOGGER.debug("Can't get shopping cart {}", ex);
            throw ex;
        }
    }
}
