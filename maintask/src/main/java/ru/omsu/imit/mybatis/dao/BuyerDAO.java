package ru.omsu.imit.mybatis.dao;

import ru.omsu.imit.mybatis.model.Buyer;

import java.util.List;

public interface BuyerDAO {
    public Buyer insert(Buyer buyer);

    public void deleteAll();

    public Buyer getById(int id);

    public List<Buyer> getAll();

    public void deleteById(int id);


}
