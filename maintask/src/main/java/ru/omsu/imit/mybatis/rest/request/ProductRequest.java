package ru.omsu.imit.mybatis.rest.request;

public class ProductRequest {
    private int productId;
    private int departmentId;
    private String productName;
    private String productDescription;
    private int quantityInStock;
    private int priceForOne;

    public ProductRequest(int productId, int departmentId, String productName, String productDescription, int quantityInStock, int priceForOne) {
        super();
        this.productId = productId;
        this.departmentId = departmentId;
        this.productName = productName;
        this.productDescription = productDescription;
        this.quantityInStock = quantityInStock;
        this.priceForOne = priceForOne;
    }

    public ProductRequest(int departmentId, String productName, String productDescription, int quantityInStock, int priceForOne) {
        this(0, departmentId, productName, productDescription, quantityInStock, priceForOne);
    }

    public String getProductName() {
        return productName;
    }


    public String getProductDescription() {
        return productDescription;
    }

    public int getQuantityInStock() {
        return quantityInStock;
    }


    public int getPriceForOne() {
        return priceForOne;
    }


    public int getProductId() {
        return productId;
    }

    public int getDepartmentId() {
        return departmentId;
    }


}
