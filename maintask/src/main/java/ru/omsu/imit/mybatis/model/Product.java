package ru.omsu.imit.mybatis.model;

public class Product {

    private int id;
    private int idDepartment;
    private String productName;
    private String productDescription;
    private int quantityInStock;
    private int priceForOne;

    public Product(int id, int idDepartment, String productName, String productDescription, int quantityInStock, int priceForOne) {
        super();
        this.id = id;
        this.idDepartment = idDepartment;
        this.productName = productName;
        this.productDescription = productDescription;
        this.quantityInStock = quantityInStock;
        this.priceForOne = priceForOne;
    }

    public Product(int idDepartment, String productName, String productDescription, int quantityInStock, int priceForOne) {
        this(0, idDepartment, productName, productDescription, quantityInStock, priceForOne);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdDepartment() {
        return idDepartment;
    }

    public void setIdDepartment(int idDepartment) {
        this.idDepartment = idDepartment;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public int getQuantityInStock() {
        return quantityInStock;
    }

    public void setQuantityInStock(int quantityInStock) {
        this.quantityInStock = quantityInStock;
    }

    public int getPriceForOne() {
        return priceForOne;
    }

    public void setPriceForOne(int priceForOne) {
        this.priceForOne = priceForOne;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        if (id != product.id) return false;
        if (idDepartment != product.idDepartment) return false;
        if (quantityInStock != product.quantityInStock) return false;
        if (priceForOne != product.priceForOne) return false;
        if (productName != null ? !productName.equals(product.productName) : product.productName != null) return false;
        return productDescription != null ? productDescription.equals(product.productDescription) : product.productDescription == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + idDepartment;
        result = 31 * result + (productName != null ? productName.hashCode() : 0);
        result = 31 * result + (productDescription != null ? productDescription.hashCode() : 0);
        result = 31 * result + quantityInStock;
        result = 31 * result + priceForOne;
        return result;
    }
}
