package ru.omsu.imit.mybatis.server;

import org.glassfish.jersey.jetty.JettyHttpContainerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.mybatis.server.config.MyServerResourceConfig;
import ru.omsu.imit.mybatis.server.config.Settings;
import ru.omsu.imit.mybatis.utils.MyBatisUtils;

import javax.ws.rs.core.UriBuilder;
import java.net.URI;

public class MyServer {

    private static final Logger LOGGER = LoggerFactory.getLogger(MyServer.class);

    private static org.eclipse.jetty.server.Server jettyServer;

    private static void attachShutDownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                stopServer();
            }
        });
    }

    public static void createServer() {
        URI baseUri = UriBuilder.fromUri("http://localhost/").port(Settings.getRestHTTPPort()).build();
        MyServerResourceConfig config = new MyServerResourceConfig();
        jettyServer = JettyHttpContainerFactory.createServer(baseUri, config);
        MyBatisUtils.initSqlSessionFactory();
        LOGGER.info("Server started at port " + Settings.getRestHTTPPort());
    }

    public static void stopServer() {
        LOGGER.info("Stopping server");
        try {
            jettyServer.stop();
            jettyServer.destroy();
        } catch (Exception e) {
            LOGGER.error("Error stopping service", e);
            System.exit(1);
        }
        LOGGER.info("Server stopped");
    }


    public static void main(String[] args) {
        attachShutDownHook();
        createServer();
    }


}
