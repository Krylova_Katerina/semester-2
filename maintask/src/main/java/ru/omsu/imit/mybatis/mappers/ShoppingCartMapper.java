package ru.omsu.imit.mybatis.mappers;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import ru.omsu.imit.mybatis.model.Buyer;
import ru.omsu.imit.mybatis.model.Product;

import java.util.List;

public interface ShoppingCartMapper {

    @Insert("INSERT INTO shoppingCart (idProduct, idBuyer, quantity) VALUES " +
            "( #{product.id}, #{buyer.id} )")
    public Integer insert(@Param("product") Product product, @Param("buyer") Buyer buyer);

    @Select("SELECT id, idProduct, idBuyer, quantity FROM shoppingCart JOIN buyer ON buyer.id=buyer.id " +
            "JOIN product ON idProduct=product.id WHERE buyer.id= {#buyerId}")
    List<Product> getByBuyerId(@Param("buyerId") int buyerId);

    @Select("SELECT id, idProduct, idBuyer, quantity FROM shoppingCart JOIN buyer ON buyer.id=buyer.id " +
            "JOIN product ON idProduct=product.id WHERE product.id= {#productId}")
    List<Product> getByProductId(@Param("productId") int productId);

    @Delete("DELETE FROM shoppingCart")
    void deleteAll();

  }
