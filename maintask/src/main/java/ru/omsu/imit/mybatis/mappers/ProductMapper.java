package ru.omsu.imit.mybatis.mappers;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;
import ru.omsu.imit.mybatis.model.Product;

import java.util.List;

public interface ProductMapper {

    @Insert("INSERT INTO product (idDepartment, productName, productDescription, quantityInStock, priceForOne) " +
            "VALUES ( #{idDepartment}, #{productName}, #{productDescription}, #{quantityInStock}, #{priceForOne})")
    @Options(useGeneratedKeys = true)
    public Integer insert(Product product);


    @Select("SELECT idDepartment, productName, productDescription, quantityInStock, priceForOne FROM product WHERE id = #{id}")
    public Product getById(int id);

    @Delete("DELETE FROM Product")
    public void deleteAll();

    @Delete("DELETE FROM Product WHERE id = #{id}")
    public int deleteById(@Param("id") int id);

    @Select("SELECT id, productName, productDescription, quantityInStock, priceForOne FROM product")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "shoppingCart", column = "id", javaType = List.class,
                    many = @Many(select = "ru.omsu.imit.mybatis.mappers.ShoppingCartMapper.getByProductId", fetchType = FetchType.LAZY)),
            @Result(property = "department", column = "id", javaType = List.class,
                    many = @Many(select = "ru.omsu.imit.mybatis.mappers.DepartmentMapper.getById", fetchType = FetchType.LAZY))
    })
    List<Product> getAll();



    @Select("SELECT id, productName, productDescription, quantityInStock, priceForOne FROM product WHERE departmentId = #{departmentId}")
    List<Product> getByDepartmentId(@Param("departmentId") int departmentId);

   /* @Select("SELECT id, productName, productName, productDescription, quantityInStock, priceForOne FROM product WHERE departmentId = #{departmentId}")
    List<Product> getByBuyerId(@Param("departmentId") int departmentId);*/

    @Select("SELECT id, idProduct, idBuyer, quantity FROM shoppingCart JOIN buyer ON buyer.id=buyer.id " +
            "JOIN product ON idProduct=product.id WHERE buyer.id= {#buyerId}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "buyer", column = "id", javaType = List.class,
                    many = @Many(select = "ru.omsu.imit.mybatis.mappers.BuyerMapper.getByProductId", fetchType = FetchType.LAZY))})
    List<Product> getByBuyerId(@Param("buyerId") int buyerId);


}
