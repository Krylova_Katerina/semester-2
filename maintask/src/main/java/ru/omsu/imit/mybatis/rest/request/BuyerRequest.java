package ru.omsu.imit.mybatis.rest.request;

import ru.omsu.imit.mybatis.model.Product;

import java.util.ArrayList;
import java.util.List;

public class BuyerRequest {
    private String buyerName;
    private List<Product> shoppingCartList;

    public BuyerRequest(String buyerName, List<Product> shoppingCartList) {
        super();
        this.buyerName = buyerName;
        this.shoppingCartList = shoppingCartList;
    }

    public BuyerRequest(String buyerName) {
        this(buyerName, new ArrayList<>());
    }

    public String getBuyerName() {
        return buyerName;
    }

    public List<Product> getShoppingCartList() {
        return shoppingCartList;
    }
}
