package ru.omsu.imit.mybatis.rest.mappers;

import ru.omsu.imit.mybatis.exception.MyException;
import ru.omsu.imit.mybatis.utils.ErrorCode;
import ru.omsu.imit.mybatis.utils.JsonUtils;

import javax.ws.rs.NotAllowedException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;


@Provider
public class MethodNotAllowedExceptionMapper implements ExceptionMapper<NotAllowedException> {

    @Override
    public Response toResponse(NotAllowedException exception) {
        return JsonUtils.failureResponse(Status.METHOD_NOT_ALLOWED, new MyException(ErrorCode.METHOD_NOT_ALLOWED));
    }
}