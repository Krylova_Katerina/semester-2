package ru.omsu.imit.mybatis.rest.response;

import ru.omsu.imit.mybatis.model.Product;

import java.util.ArrayList;
import java.util.List;

public class BuyerResponse {
    private int id;
    private String buyerName;
    private List<Product> shoppingCartList;

    public BuyerResponse(int id, String buyerName, List<Product> shoppingCartList) {
        super();
        this.id = id;
        this.buyerName = buyerName;
        this.shoppingCartList = shoppingCartList;
    }

    public BuyerResponse(int id, String buyerName) {
        this(id, buyerName, new ArrayList<>());
    }

    public int getId() {
        return id;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public List<Product> getShoppingCartList() {
        return shoppingCartList;
    }
}
