package ru.omsu.imit.mybatis.resources;

import ru.omsu.imit.mybatis.exception.MyException;
import ru.omsu.imit.mybatis.service.BuyerService;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;


@Path("/api")
public class BuyerResource {


    private static BuyerService buyerService = new BuyerService();

    @POST
    @Path("/shop/buyer")
    @Consumes("application/json")
    @Produces("application/json")
    public Response addBuyer(String json) throws MyException {
        return buyerService.insertBuyer(json);
    }

    @GET
    @Path("/shop/buyer/{id}")
    @Produces("application/json")
    public Response getById(@PathParam(value = "id") int id) {
        return buyerService.getById(id);
    }


    @GET
    @Path("/shop/buyer")
    @Produces("application/json")
    public Response getAll() {
        return buyerService.getAll();
    }


    @DELETE
    @Path("/shop/buyer/{id}")
    @Produces("application/json")
    public Response deleteById(@PathParam(value = "id") int id, String json) {
        return buyerService.deleteById(id, json);
    }

}
