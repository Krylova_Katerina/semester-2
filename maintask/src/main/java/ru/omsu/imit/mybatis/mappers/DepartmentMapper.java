package ru.omsu.imit.mybatis.mappers;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;
import ru.omsu.imit.mybatis.model.Department;

import java.util.List;

public interface DepartmentMapper {
    @Insert("INSERT INTO department (departmentName) VALUES ( #{departmentName})")
    @Options(useGeneratedKeys = true)
    public Integer insert(Department department);


    @Select("SELECT id, departmentName FROM department WHERE id = #{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "product", column = "id", javaType = List.class,
                    many = @Many(select = "ru.omsu.imit.mybatis.mappers.ProductMapper.getByDepartmentId", fetchType = FetchType.LAZY)
            )})
    public Department getById(int id);

    @Delete("DELETE FROM department")
    public void deleteAll();

    @Select("SELECT id, buyerName FROM buyer")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "product", column = "id", javaType = List.class,
                    many = @Many(select = "ru.omsu.imit.mybatis.mappers.ProductMapper.getById", fetchType = FetchType.LAZY)
            )})
    List<Department> getAll();

    @Delete("DELETE FROM department WHERE id = #{id}")
    void delete(@Param("id") int id);
}
