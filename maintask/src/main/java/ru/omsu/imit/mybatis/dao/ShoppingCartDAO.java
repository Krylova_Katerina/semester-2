package ru.omsu.imit.mybatis.dao;

import ru.omsu.imit.mybatis.exception.MyException;
import ru.omsu.imit.mybatis.model.Buyer;
import ru.omsu.imit.mybatis.model.Product;

import java.util.List;

public interface ShoppingCartDAO {

    public void insert(Product product, Buyer buyer);


    public List<Product> getByBuyerId(int buyerId) throws MyException;

    public List<Product> getByProductId(int productId) throws MyException;

    public void deleteAll();


}
