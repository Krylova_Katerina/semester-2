package ru.omsu.imit.mybatis.mappers;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;
import ru.omsu.imit.mybatis.model.Buyer;
import ru.omsu.imit.mybatis.model.Product;

import java.util.List;

public interface BuyerMapper {
    @Insert("INSERT INTO buyer (buyerName) " +
            "VALUES ( #{buyerName})")
    @Options(useGeneratedKeys = true)
    public Integer insert(Buyer buyer);


    @Select("SELECT id, buyerName FROM buyer WHERE id = #{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "shoppingCart", column = "id", javaType = List.class,
                    many = @Many(select = "ru.omsu.imit.mybatis.mappers.ShoppingCartMapper.getByBuyerId", fetchType = FetchType.LAZY)
            )})
    public Buyer getById(int id);

    @Select("SELECT * FROM product" +
            "WHERE id IN (SELECT idBuyer FROM shoppingCart WHERE idProduct = #{idProduct})")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "product", column = "id", javaType = List.class,
                    many = @Many(select = "ru.omsu.imit.mybatis.mappers.ProductMapper.getByBuyerId", fetchType = FetchType.LAZY))})
    public List<Product> getByProductId(@Param("idProduct") int idProduct);


    @Delete("DELETE FROM buyer")
    public void deleteAll();

    @Select("SELECT id, buyerName FROM buyer")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "shoppingCart", column = "id", javaType = List.class,
                    many = @Many(select = "ru.omsu.imit.mybatis.mappers.ShoppingCartMapper.getByBuyerId", fetchType = FetchType.LAZY)
            )})
    List<Buyer> getAll();

    @Delete("DELETE FROM buyer WHERE id = #{id}")
    void delete(@Param("id") int id);
}
