package ru.omsu.imit.mybatis.server.config;

public class MyServerResourceConfig extends org.glassfish.jersey.server.ResourceConfig {

    public MyServerResourceConfig() {
        packages("ru.omsu.imit.mybatis.resources",
                "ru.omsu.imit.mybatis.rest.mappers");
    }

}
