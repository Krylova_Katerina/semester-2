package ru.omsu.imit.mybatis.resources;

import ru.omsu.imit.mybatis.exception.MyException;
import ru.omsu.imit.mybatis.service.DepartmentService;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/api")
public class DepartmentResource {
    private static DepartmentService departmentService = new DepartmentService();

    @POST
    @Path("/shop/department")
    @Consumes("application/json")
    @Produces("application/json")
    public Response addDepartment(String json) throws MyException {
        return departmentService.insertDepartment(json);
    }

    @GET
    @Path("/shop/department/{id}")
    @Produces("application/json")
    public Response getById(@PathParam(value = "id") int id) {
        return departmentService.getById(id);
    }

    @GET
    @Path("/shop/department")
    @Produces("application/json")
    public Response getAll() {
        return departmentService.getAll();
    }


    @DELETE
    @Path("/shop/department/{id}")
    @Produces("application/json")
    public Response deleteById(@PathParam(value = "id") int id, String json) {
        return departmentService.deleteById(id, json);
    }
}
