package ru.omsu.imit.mybatis.daoimpl;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.mybatis.dao.BuyerDAO;
import ru.omsu.imit.mybatis.model.Buyer;

import java.util.List;

public class BuyerDAOImpl extends BaseDAOImpl implements BuyerDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(BaseDAOImpl.class);


    @Override
    public Buyer insert(Buyer buyer) {
        LOGGER.debug("DAO Insert buyer {}", buyer);
        try (SqlSession sqlSession = getSession()) {
            try {
                getBuyerMapper(sqlSession).insert(buyer);
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't insert buyer {} {}", buyer, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
        return buyer;
    }

    @Override
    public void deleteAll() {
        LOGGER.debug("DAO Delete All Buyers {}");
        try (SqlSession sqlSession = getSession()) {
            try {
                getBuyerMapper(sqlSession).deleteAll();
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't clear all Buyers {}", ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public Buyer getById(int id) {
        try (SqlSession sqlSession = getSession()) {
            return getBuyerMapper(sqlSession).getById(id);
        } catch (RuntimeException ex) {
            LOGGER.debug("Can't get Buyer by Id {} {}", id, ex);
            throw ex;
        }
    }

    @Override
    public List<Buyer> getAll() {
        try (SqlSession sqlSession = getSession()) {
            return getBuyerMapper(sqlSession).getAll();
        } catch (RuntimeException ex) {
            LOGGER.debug("Can't get all Buyers {}", ex);
            throw ex;
        }

    }

    @Override
    public void deleteById(int id) {
        LOGGER.debug("Delete buyer by ID {}", id);
        try (SqlSession sqlSession = getSession()) {
            try {
                getBuyerMapper(sqlSession).delete(id);
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't clear buyer by id {}", ex);
                sqlSession.rollback();
                throw ex;
            }
        }
    }

}
