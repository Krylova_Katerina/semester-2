package ru.omsu.imit.mybatis.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.mybatis.dao.BuyerDAO;
import ru.omsu.imit.mybatis.dao.DepartmentDAO;
import ru.omsu.imit.mybatis.dao.ProductDAO;
import ru.omsu.imit.mybatis.daoimpl.BuyerDAOImpl;
import ru.omsu.imit.mybatis.daoimpl.DepartmentDAOImpl;
import ru.omsu.imit.mybatis.daoimpl.ProductDAOImpl;
import ru.omsu.imit.mybatis.rest.response.EmptySuccessResponse;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


public class CommonService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommonService.class);
    private static final Gson GSON = new GsonBuilder().create();
    private static BuyerDAO buyerDAO = new BuyerDAOImpl();
    private static DepartmentDAO departmentDAODAO = new DepartmentDAOImpl();
    private static ProductDAO productDAO = new ProductDAOImpl();


    public static Response clear() {
        LOGGER.debug("clear All");
        buyerDAO.deleteAll();
        departmentDAODAO.deleteAll();
        productDAO.deleteAll();
        String response = GSON.toJson(new EmptySuccessResponse());
        return Response.ok(response, MediaType.APPLICATION_JSON).build();
    }
}
