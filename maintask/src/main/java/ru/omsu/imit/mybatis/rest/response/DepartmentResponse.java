package ru.omsu.imit.mybatis.rest.response;

import ru.omsu.imit.mybatis.model.Product;

import java.util.ArrayList;
import java.util.List;

public class DepartmentResponse {
    private int id;
    private String departmentName;
    private List<Product> productList;

    public DepartmentResponse(int id, String departmentName, List<Product> productList) {
        super();
        this.id = id;
        this.departmentName = departmentName;
        this.productList = productList;
    }

    public DepartmentResponse(int id, String departmentName) {
        this(id, departmentName, new ArrayList<>());
    }


    public int getId() {
        return id;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public List<Product> getProductList() {
        return productList;
    }
}
