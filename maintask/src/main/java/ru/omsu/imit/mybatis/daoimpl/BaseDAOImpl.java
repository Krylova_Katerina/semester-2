package ru.omsu.imit.mybatis.daoimpl;

import org.apache.ibatis.session.SqlSession;
import ru.omsu.imit.mybatis.mappers.*;
import ru.omsu.imit.mybatis.utils.MyBatisUtils;

public class BaseDAOImpl {

    protected SqlSession getSession() {
        return MyBatisUtils.getSqlSessionFactory().openSession();
    }

    protected BuyerMapper getBuyerMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(BuyerMapper.class);
    }

    protected DepartmentMapper getDepartmentMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(DepartmentMapper.class);
    }

    protected ProductMapper getProductMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(ProductMapper.class);
    }

    protected ShoppingCartMapper getShoppingCartMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(ShoppingCartMapper.class);
    }
}