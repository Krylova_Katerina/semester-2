package ru.omsu.imit.mybatis.utils;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import org.apache.commons.lang.StringUtils;
import ru.omsu.imit.mybatis.exception.MyException;
import ru.omsu.imit.mybatis.rest.response.FailureResponse;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

public class JsonUtils {

    private static final Gson GSON = new Gson();

    public static <T> T getClassInstanceFromJson(Gson gson, String json, Class<T> clazz) throws MyException {
        if (StringUtils.isEmpty(json)) {
            throw new MyException(ErrorCode.NULL_REQUEST);
        }
        try {
            return gson.fromJson(json, clazz);
        } catch (JsonSyntaxException ex) {
            throw new MyException(ErrorCode.JSON_PARSE_EXCEPTION, json);
        }
    }

    public static Response failureResponse(Status status, MyException ex) {
        return Response.status(status).entity(GSON.toJson(new FailureResponse(ex.getErrorCode(), ex.getMessage()))).build();
    }

    public static Response failureResponse(MyException ex) {
        return failureResponse(Status.BAD_REQUEST, ex);
    }

}
