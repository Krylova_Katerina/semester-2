package ru.omsu.imit.mybatis.resources;

import ru.omsu.imit.mybatis.service.CommonService;

import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
@Path("/api")
public class CommonResource {
    @DELETE
    @Path("/clearDB")
    @Produces("application/json")
    public Response delete() {
        return CommonService.clear();
    }
}
