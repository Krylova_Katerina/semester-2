package ru.omsu.imit.mybatis.dao;

import ru.omsu.imit.mybatis.exception.MyException;
import ru.omsu.imit.mybatis.model.Product;

import java.util.List;

public interface ProductDAO {
    public Product insert(Product product);

    public void deleteAll();

    public Product getById(int id);

    void deleteById(int id);

    public List<Product> getByDepartmentId(int departmentId);

    public List<Product> getByBuyerId(int buyerId) throws MyException;

    public List<Product> getAll();
}
