package ru.omsu.imit.mybatis.rest.mappers;

import ru.omsu.imit.mybatis.exception.MyException;
import ru.omsu.imit.mybatis.utils.ErrorCode;
import ru.omsu.imit.mybatis.utils.JsonUtils;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;


@Provider
public class WrongURLExceptionMapper implements ExceptionMapper<NotFoundException> {

    @Override
    public Response toResponse(NotFoundException exception) {
        return JsonUtils.failureResponse(Status.NOT_FOUND, new MyException(ErrorCode.WRONG_URL));
    }
}