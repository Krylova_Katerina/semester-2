package ru.omsu.imit.mybatis.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.mybatis.dao.DepartmentDAO;
import ru.omsu.imit.mybatis.daoimpl.DepartmentDAOImpl;
import ru.omsu.imit.mybatis.exception.MyException;
import ru.omsu.imit.mybatis.model.Department;
import ru.omsu.imit.mybatis.rest.request.DepartmentRequest;
import ru.omsu.imit.mybatis.rest.response.DepartmentResponse;
import ru.omsu.imit.mybatis.rest.response.EmptySuccessResponse;
import ru.omsu.imit.mybatis.utils.JsonUtils;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

public class DepartmentService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DepartmentService.class);
    private static final Gson GSON = new GsonBuilder().create();
    private DepartmentDAO departmentDAO = new DepartmentDAOImpl();

    public Response insertDepartment(String json) throws MyException {
        LOGGER.debug("Insert Department " + json);
        try {
            DepartmentRequest request = JsonUtils.getClassInstanceFromJson(GSON, json, DepartmentRequest.class);
            Department department = new Department(request.getDepartmentName(), request.getProductList());
            Department addDepartment = departmentDAO.insert(department);
            String response = GSON.toJson(new DepartmentResponse(addDepartment.getId(), addDepartment.getDepartmentName(), addDepartment.getProductList()));
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (MyException ex) {
            return JsonUtils.failureResponse(ex);
        }
    }

    public Response getById(int id) {
        LOGGER.debug("Get Department by ID " + id);
        Department department = departmentDAO.getById(id);
        String response = GSON.toJson(new DepartmentResponse(department.getId(), department.getDepartmentName(), department.getProductList()));
        return Response.ok(response, MediaType.APPLICATION_JSON).build();

    }

    public Response getAll() {
        LOGGER.debug("get All Department");
        List<Department> departments = departmentDAO.getAll();
        List<DepartmentResponse> responseList = new ArrayList<>();
        for (Department department : departments)
            responseList.add(new DepartmentResponse(department.getId(), department.getDepartmentName(), department.getProductList()));
        String respose = GSON.toJson(responseList);
        return Response.ok(respose, MediaType.APPLICATION_JSON).build();
    }

    public Response deleteById(int id, String json) {
        LOGGER.debug("Delete Department by ID");
        departmentDAO.deleteById(id);
        String response = GSON.toJson(new EmptySuccessResponse());
        return Response.ok(response, MediaType.APPLICATION_JSON).build();
    }

}
