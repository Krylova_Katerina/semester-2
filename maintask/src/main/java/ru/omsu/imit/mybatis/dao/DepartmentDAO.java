package ru.omsu.imit.mybatis.dao;

import ru.omsu.imit.mybatis.model.Department;

import java.util.List;

public interface DepartmentDAO {
    public Department insert(Department department);

    public void deleteAll();

    public Department getById(int id);

    public void deleteById(int id);

    public List<Department> getAll();
}