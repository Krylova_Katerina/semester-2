package ru.omsu.imit.mybatis.rest.mappers;

import com.fasterxml.jackson.core.JsonProcessingException;
import ru.omsu.imit.mybatis.exception.MyException;
import ru.omsu.imit.mybatis.utils.ErrorCode;
import ru.omsu.imit.mybatis.utils.JsonUtils;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;


@Provider
public class JsonProcessingExceptionMapper implements
        ExceptionMapper<JsonProcessingException> {
    @Override
    public Response toResponse(JsonProcessingException exception) {
        return JsonUtils.failureResponse(Status.BAD_REQUEST, new MyException(ErrorCode.JSON_PARSE_EXCEPTION));
    }
}