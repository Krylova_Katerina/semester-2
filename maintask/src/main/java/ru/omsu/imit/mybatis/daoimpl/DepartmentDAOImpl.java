package ru.omsu.imit.mybatis.daoimpl;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.mybatis.dao.DepartmentDAO;
import ru.omsu.imit.mybatis.model.Department;

import java.util.List;

public class DepartmentDAOImpl extends BaseDAOImpl implements DepartmentDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(BaseDAOImpl.class);

    @Override
    public Department insert(Department department) {
        LOGGER.debug("DAO Insert department {}", department);
        try (SqlSession sqlSession = getSession()) {
            try {
                getDepartmentMapper(sqlSession).insert(department);
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't insert department {} {}", department, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
        return department;
    }

    @Override
    public void deleteAll() {
        LOGGER.debug("DAO Delete All Department {}");
        try (SqlSession sqlSession = getSession()) {
            try {
                getDepartmentMapper(sqlSession).deleteAll();
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't clear all Department {}", ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public Department getById(int id) {
        try (SqlSession sqlSession = getSession()) {
            return getDepartmentMapper(sqlSession).getById(id);
        } catch (RuntimeException ex) {
            LOGGER.debug("Can't get department by Id {} {}", id, ex);
            throw ex;
        }
    }

    @Override
    public List<Department> getAll() {
        try (SqlSession sqlSession = getSession()) {
            return getDepartmentMapper(sqlSession).getAll();
        } catch (RuntimeException ex) {
            LOGGER.debug("Can't get all Departments {}", ex);
            throw ex;
        }

    }

    @Override
    public void deleteById(int id) {
        LOGGER.debug("Delete Department by ID {}", id);
        try (SqlSession sqlSession = getSession()) {
            try {
                getDepartmentMapper(sqlSession).delete(id);
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't clear Department by id {}", ex);
                sqlSession.rollback();
                throw ex;
            }
        }
    }
}
