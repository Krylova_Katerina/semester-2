package ru.omsu.imit.mybatis.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Buyer {
    private int id;
    private String buyerName;
    private List<Product> productList;

    public Buyer(int id, String buyerName, List<Product> productList) {
        super();
        this.id = id;
        this.buyerName = buyerName;
        this.productList = productList;
    }

    public Buyer(String buyerName, List<Product> productList) {
        this(0, buyerName, productList);
    }

    public Buyer(String buyerName) {
        this(0, buyerName, new ArrayList<>());
    }

    public Buyer(int id, String buyerName) {
        this(id, buyerName, new ArrayList<>());
    }


    public Buyer() {

    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Buyer buyer = (Buyer) o;
        return id == buyer.id &&
                Objects.equals(buyerName, buyer.buyerName) &&
                Objects.equals(productList, buyer.productList);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, buyerName, productList);
    }

    @Override
    public String toString() {
        return "Buyer{" +
                "id=" + id +
                ", buyerName='" + buyerName + '\'' +
                ", productList=" + productList +
                '}';
    }
}
