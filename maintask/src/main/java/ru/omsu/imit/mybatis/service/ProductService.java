package ru.omsu.imit.mybatis.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.mybatis.dao.ProductDAO;
import ru.omsu.imit.mybatis.daoimpl.ProductDAOImpl;
import ru.omsu.imit.mybatis.exception.MyException;
import ru.omsu.imit.mybatis.model.Product;
import ru.omsu.imit.mybatis.rest.request.ProductRequest;
import ru.omsu.imit.mybatis.rest.response.EmptySuccessResponse;
import ru.omsu.imit.mybatis.rest.response.ProductResponse;
import ru.omsu.imit.mybatis.utils.JsonUtils;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;


public class ProductService {


    private static final Logger LOGGER = LoggerFactory.getLogger(ProductService.class);
    private static final Gson GSON = new GsonBuilder().create();
    private ProductDAO productDao = new ProductDAOImpl();

    public Response insert(String json) {
        LOGGER.debug("Insert Product " + json);
        try {
            ProductRequest request = JsonUtils.getClassInstanceFromJson(GSON, json, ProductRequest.class);
            Product product = new Product(request.getProductId(), request.getDepartmentId(), request.getProductName(),
                    request.getProductDescription(), request.getQuantityInStock(), request.getPriceForOne());
            Product addProduct = productDao.insert(product);
            String response = GSON.toJson(new ProductResponse(addProduct.getId(), addProduct.getIdDepartment(),
                    addProduct.getProductName(), addProduct.getProductDescription(), addProduct.getQuantityInStock(),
                    addProduct.getPriceForOne()));
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (MyException ex) {
            return JsonUtils.failureResponse(ex);
        }
    }

    public Response getById(int id) {
        LOGGER.debug("Get product by id " + id);
        Product product = productDao.getById(id);
        String response = GSON.toJson(new ProductResponse(product.getId(), product.getIdDepartment(), product.getProductName(),
                product.getProductDescription(), product.getQuantityInStock(), product.getPriceForOne()));
        return Response.ok(response, MediaType.APPLICATION_JSON).build();
    }


    public Response deleteById(int id, String json) {
        LOGGER.debug("clear By id " + id);
        productDao.deleteById(id);
        String response = GSON.toJson(new EmptySuccessResponse());
        return Response.ok(response, MediaType.APPLICATION_JSON).build();
    }


    public Response getByDepartmentId(int departmentId) {
        LOGGER.debug("Get product by department's ID " + departmentId);
        List<Product> products = productDao.getByDepartmentId(departmentId);
        List<ProductResponse> responseList = new ArrayList<>();
        for (Product product : products)
            responseList.add(new ProductResponse(product.getId(), product.getIdDepartment(), product.getProductName(),
                    product.getProductDescription(), product.getQuantityInStock(), product.getPriceForOne()));
        String response = GSON.toJson(responseList);
        return Response.ok(response, MediaType.APPLICATION_JSON).build();
    }

    public Response getAll() {
        LOGGER.debug("get All products");
        List<Product> products = productDao.getAll();
        List<ProductResponse> responseList = new ArrayList<>();
        for (Product product : products)
            responseList.add(new ProductResponse(product.getId(), product.getIdDepartment(), product.getProductName(),
                    product.getProductDescription(), product.getQuantityInStock(), product.getPriceForOne()));
        String response = GSON.toJson(responseList);
        return Response.ok(response, MediaType.APPLICATION_JSON).build();
    }


    public Response getByBuyerId(int id) {
        LOGGER.debug("Get product by department's ID " + id);
        try {
            List<Product> products = productDao.getByBuyerId(id);
            List<ProductResponse> responseList = new ArrayList<>();
            for (Product product : products)
                responseList.add(new ProductResponse(product.getId(), product.getIdDepartment(), product.getProductName(),
                        product.getProductDescription(), product.getQuantityInStock(), product.getPriceForOne()));
            String response = GSON.toJson(responseList);
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (MyException ex) {
            ex.printStackTrace();
            return JsonUtils.failureResponse(ex);
        }
    }
}
