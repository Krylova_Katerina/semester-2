package ru.omsu.imit.mybatis.resources;

import ru.omsu.imit.mybatis.service.ProductService;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/api")
public class ProductResource {
    private static ProductService productService = new ProductService();

    @POST
    @Path("/shop/product")
    @Consumes("application/json")
    @Produces("application/json")
    public Response addTodoItem(String json) {
        return productService.insert(json);
    }

    @GET
    @Path("/shop/product/{id}")
    @Produces("application/json")
    public Response getById(@PathParam(value = "id") int id) {
        return productService.getById(id);
    }

    @GET
    @Path("/shop/product")
    @Produces("application/json")
    public Response getAll(@PathParam(value = "id") int id) {
        return productService.getAll();
    }

    @GET
    @Path("/shop/product/department/{id}")
    @Produces("application/json")
    public Response getByDepartmentId(@PathParam(value = "id") int id) {
        return productService.getByDepartmentId(id);
    }



//    @GET
//    @Path("/shop/product/{id}")
//    @Produces("application/json")
//    public Response getByBuyerId(@PathParam(value = "id") int id) {
//        return productService.getByBuyerId(id);
//    }


    @DELETE
    @Path("/shop/product/{id}")
    @Produces("application/json")
    public Response deleteById(@PathParam(value = "id") int id, String json) {
        return productService.deleteById(id, json);
    }
}
