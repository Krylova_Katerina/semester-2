package ru.omsu.imit.mybatis.exception;

import ru.omsu.imit.mybatis.utils.ErrorCode;

public class MyException extends Exception {


    private ErrorCode errorCode;
    private String param;

    public MyException(ErrorCode errorCode, String param) {
        this.errorCode = errorCode;
        this.param = param;
    }

    public MyException(ErrorCode errorCode) {
        this(errorCode, null);
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    public String getMessage() {
        if (param != null)
            return String.format(errorCode.getMessage(), param);
        else
            return errorCode.getMessage();
    }

}
